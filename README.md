# 𝔾raviton-web
*Entity-Component-System* data explorer. The aim is to be able to explore the data of tangible and intangible entities in a consistent, fast and accurate way. It must be possible to merge very heterogeneous data thanks to the *trait* description (see rust language). Uses human powered vehicle (velomobile) data as the initial set. 

[https://graviton.frama.io/graviton-web](https://graviton.frama.io/graviton-web)

[work in progress..]
## Functionality
- multi language interface and data properties using css variables
- quick navigation in database
- offline data navigation
- data filtering
- local backup with localstorage
- export of data to a file
- keyboard shortcuts
- configurable by url
## Acknowledgements
- Initial code from [vm-slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html) by Jockel Hofmann.
- Data compilation of velomobile by Christoph Moder in the [Velomobil-Grundwissen](https://gitlab.com/cmoder/velomobil-grundwissen) project
- Samural for help with the French translation
- All velomobilistsfrom the [www.velomobilforum.de](https://www.velomobilforum.de/) and [velorizontal](http://velorizontal.1fr1.net) forums who have measured and shared their data
- [Framasoft](https://framasoft.org/) for hosting the forge and all the actions carried out for web neutrality
