'use strict';{const data_append = [

// FUNCTIONS
{	F				: true,
	n				: "addKeyboardListener",
	desc			: "Add keyboard listener (keyup and keydown)|en",
	lang			: "es",
},
{	F				: true,
	n				: "addClickListener",
	desc			: "Add mouse click listener|en",
	lang			: "es",
},
{	F				: true,
	n				: "addMousedownListener",
	desc			: "Add mouse down listener|en",
	lang			: "es",
},
{	F				: true,
	n				: "addScrollListener",
	desc			: "Add mouse scroll listener|en",
	lang			: "es",
},
{	F				: true,
	n				: "domwindow",
	desc			: "return window dom|en",
	lang			: "es",
	code			: "return window;",
},

// TRAITS
{	T				: "listenkeyboard",
	keyup			: {},
	keydown			: {},
	nbcall			: { defaultvalue : 0 },
	from			: { defaultvalue : "domwindow" }, // root window for keyboard listener INACTIVE for moment
},
{	T				: "listenmouse",
	click			: {},
	mousedown		: {},
	scroll			: {},
	textlabel		: { defaultvalue : "=e.identify.n" },
	backgroundimage	: {},
	from			: { defaultvalue : "='btn-'+e.identify.n" },
},
{	T				: "listenchange",
	from			: { defaultvalue : "='opt-'+e.identify.n" },
	bool			: {},
},

{	Q				: "keyboard", // list of shortcuts
	filter			: [ { equals : { 'listenkeyboard': true } } ],
//	filter			: [ { equals : { "listenkeyboard.nbcall": "0","listenmouse": true, } } ],
//	filter			: [ { iequals : { "identify.n": "moveir","listenmouse": true, } } ],
	sorted			: [ {"listenkeyboard.nbcall": "down" } ],
	summary			: {"listenkeyboard.nbcall": ["summ","max","min","mean","median","stddeviation"] },
	defaultvalue	: { "identify.desc": "='shortcut : ['+e.listenkeyboard.keyup.join('] [')+']'" },
	newprop			: { test : '' }
},
{	Q				: "mousewin", // global mouse
	filter			: [ { equals : { "listenmouse.from": "domwindow" } } ],
},

// GLOBAL VARS
{	G				: true,
	keyscactive		: true,
	lastkey			: "",
},

// DATA
{	identify			: { n	 	: "addKeyboardListener" },
	callesfn			: {	n		: "=addKeyboardListener" },
	startfn				: { order	: 1},
},
{	identify			: { n	 	: "addMousedownListener" },
	callesfn			: {	n		: "=addMousedownListener" },
	startfn				: { order	: 3},
},
{	identify			: { n	 	: "addClickListener" },
	callesfn			: {	n		: "=addClickListener" },
	startfn				: { order	: 2},
},
//{	identify			: { n	 	: "addScrollListener" },
//	callesfn			: {	n		: "=addScrollListener" },
//	startfn				: true,
//},
];FILES.app = [...FILES.app,...data_append];}

function addKeyboardListener() {

//console.log(q.keyboard[1]);
	const keyup = q.keyboard[1].hasOwnProperty('listenkeyboard.keyup');
	const keydown = q.keyboard[1].hasOwnProperty('listenkeyboard.keydown');

	if (keyup || keydown)
		window.addEventListener("keydown", keydownHandle, false);
	if (keyup)
		window.addEventListener("keyup", keyupHandle, false);

}

// TODO perenise ?
function getarr(data,prop,i=1) {
	const tmp = data[prop];
	if (typeof tmp === 'undefined') return undefined;
	else return tmp[i];
}

function keydownHandle(ev) {
	let found = false;
	const query = q.keyboard[1],
		keydown = getarr(query,'listenkeyboard.keydown'),
		keyup = getarr(query,'listenkeyboard.keyup'),
		func = getarr(query,'callesfn.n',2),
		nbcall = getarr(query,'listenkeyboard.nbcall');

//	console.log("handle",keyup,keydown,nbcall,func);

	let i = 0;
	g[0].lastkey = ev.key;
//	console.log(ev.key);

	for (const fn of func) {

		if (keydown[i]) {
			const val = keydown[i]; 
			if (val === undefined || val.length == 0) {
				if (typeof keyup !== 'undefined' && keyup[i] !== undefined && keyup[i].length != 0) {
					for (const k of keyup[i])
						if (ev.key === k && g[0].keyscactive) {
							ev.preventDefault();
							found = true;
						}
				}
			}
			else {
				for (const k of val) {

					if (k.length == 1) {
						if (!ev.ctrlKey && ev.key === k && g[0].keyscactive) {
							ev.preventDefault();
							fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
							nbcall[i]++;
							found = true;
						}
					}
					else if (k.startsWith('[')) {
						if (k.substr(1,k.indexOf(']') - 1) == 'ctrl') {
							if (ev.ctrlKey && ev.key === k.substr(k.indexOf(']') + 1)) {
								ev.preventDefault();
								fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
								nbcall[i]++;
								found = true;
							}
						}
					}
					else {
						if (!ev.ctrlKey && ev.key === k) {
							ev.preventDefault();
							fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
							nbcall[i]++;
							found = true;
						}
					}
				}
			}	
			if (found) break;
		}
		i++;
	}
}

function keyupHandle(ev) {
	let found = false;
	const query = q.keyboard[1],
		keyup = getarr(query,'listenkeyboard.keyup'),
		func = getarr(query,'callesfn.n',2),
		nbcall = getarr(query,'listenkeyboard.nbcall');

	let i = 0;
	for (const fn of func) {
		const key = keyup[i];
		if (key) {
//console.log(hastrait,key,' ',hastrait && key, hastrait ?? key)
			for (const k of key) {
				if (k.length == 1) {
					if (!ev.ctrlKey && ev.key === k && g[0].keyscactive ) {
						fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
						nbcall[i]++;
						found = true;
					}
				}
				else if (k.startsWith('[')) {
					if (k.substr(1,k.indexOf(']') - 1) == 'ctrl') {
						if (ev.ctrlKey && ev.key === k.substr(k.indexOf(']') + 1)) {
							fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
							nbcall[i]++;
							found = true;
						}
					}
				}
				else {
					if (!ev.ctrlKey && ev.key === k) {
						fn[0](...(typeof fn[1] !== 'undefined' ? fn[1] : ''));
						nbcall[i]++;
						found = true;
					}
				}
				if (found) break;
			}
			if (found) break;
		}
		i++;
	}
}

function addClickListener() {
	const query = q.mousewin[1],
		click = getarr(query,'listenmouse.click');
	if (typeof click !== 'undefined') {
		window.addEventListener("click", function(ev) {
			if (ev.ctrlKey) {
				const func = getarr(query,'callesfn.n',2);

				let i = 0;
				for (const fn of func) {
					if (click[i])
						fn[0](...(typeof fn[1] !== 'undefined' ? [ev,...fn[1]] : [ev]));
					i++;
				}
			}
		}, false);
	}
}

function addMousedownListener() {
	const query = q.mousewin[1],
		mousedown = getarr(query,'listenmouse.mousedown');
	if (typeof mousedown !== 'undefined') {
		window.addEventListener("mousedown", function(ev) {
			if (ev.ctrlKey) {
				const func = getarr(query,'callesfn.n',2);

				let i = 0;
				for (const fn of func) {
					if (mousedown[i])
						fn[0](...(typeof fn[1] !== 'undefined' ? [ev,...fn[1]] : [ev]));
					i++;
				}
			}
		}, false);
	}
}

function addScrollListener() {
//	window.addEventListener("scroll", function(ev) {
////	console.log("scroll");
////		if (ev.ctrlKey) {
//			const trait = h[p.indexOf(':listenmouse')];
//			const scroll = h[p.indexOf('scroll:listenmouse')];

//			let i = 0;
//			for (const hastrait of trait) {
//				const key = scroll[i];
//				if (hastrait && key) {
//					const func = h[p.indexOf('-fn-n:callesfn')];
//					const para = h[p.indexOf('-in-n:callesfn')];
//					if (para[i])
//						func[i](...para[i]);
//					else
//						func[i](ev);
//				}
//				i++;
//			}
////		}
//	}, false);
}
//function addChangeListener() {
//	let i = 0;
//	for (const val of fn_change) {
//		if (val) {
//			const elem = document.getElementById(fn_name[i].join(''));
//			if (elem !== null) {
//				elem.addEventListener('change', fn[i]);
//				if (fn_name[i].length == 2) elem.fn_name = fn_name[i][1];
//				elem.fn_params = fn_params[i];
//			}
//		}
//		i++;
//	}
//}
