# 𝔾raviton-web
Explorateur de données *entité-composant-système*. L'objectif est de pouvoir explorer les données d'entités matérielles et immatérielles de manière cohérente, rapide et précise. Il doit être possible de fusionner des données hétérogènes grâce à l'utilisation des *trait* (voir langage rust). Utilise des données de véhicules à propulsion humaine (vélomobile) comme données initiales. 

[https://graviton.frama.io/graviton-web](https://graviton.frama.io/graviton-web)

[travail en cours...]
## Fonctionnalité
- interface et champs de données multi-langue avec les variables css
- navigation rapide dans la base de données
- navigation hors ligne dans les données
- filtrage des données
- sauvegarde locale avec "localstorage"
- exportation de données vers un fichier
- raccourcis clavier
- configurable par url
## Remerciements
- Jockel Hofmann pour le code et l'idée initiale [vm-slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html).
- Christoph Moder pour la compilation des données sur les velomobiles dans le cadre du projet [Velomobil-Grundwissen](https://gitlab.com/cmoder/velomobil-grundwissen).
- Samural pour l'aide à la traduction française
- Tous les vélomobilistes des forums [www.velomobilforum.de](https://www.velomobilforum.de/) et [velorizontal](http://velorizontal.1fr1.net) qui ont mesuré et partagé leurs données.
- [Framasoft](https://framasoft.org/) pour l'hébergement de la forge et toutes les actions menées pour la neutralité du web
