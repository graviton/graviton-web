{const data_append = [
// TRAITS
{	T				: "identify", // trait who give a identifier prop
	n				: {"hashmaxlen": 4},
	desc			: {},
},
{	T				: "object",
	image			: {},
	material		: {},
	width			: {},
	height			: {},
	length			: {},
	volume			: { defaultvalue: "=(e.object.width * e.object.height * e.object.length) / 1000000" },
	weight			: {},
	density			: { defaultvalue: "=e.object.weight / e.object.volume" },
},
{	T				: "manufactured",
	constructor		: {},
	modele			: {},
	version			: {},
	serialnumber	: {},
},
{	T				: "rolling",
	wheels			: {},
	wheels_xyz		: {},
	wheels_brake	: {},
	wheels_steering	: {},
	wheels_motor	: {},
	handlebars		: {},

	numberwheels	: {},
	numberwheelsfront: {},
	numberwheelsrear: {},
},

// QUERY
{	Q				: "init",
},
{	Q				: "object",
	filter			: [ { equals : { "object": true } } ],
	order			: [ "identify", "object", "*" ],
//	order			: [ "object", "*" ],
},
{	Q				: "humanpoweredvehicle",
	filter			: [ { equals : { "object": true } } ],
	order			: [ "manufactured", "*" ],
//	filter			: [ { equals : { "object": true } } ],
},

{
object: {
	length: 266,
	width: 75,
	height: 93,
	weight: 35,
	image: "url('https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Flevo_alleweder-2.jpg/800px-Flevo_alleweder-2.jpg')",
},
manufactured: {
	constructor: "Alleweder",
	modele: "AW2",
},
rolling: {
	wheels: [ "wheel?size=20", "wheel?size=20", "wheel?size=24"],
	wheels_xyz: [ [-35,0,0], [35,0,0], [0,5,130] ],
	wheels_brake: [ ["drum"],["drum"], ["vbrake","motor"] ],
	wheels_steering: [ ["hinge"],["hinge"] ],
	wheels_motor: [ [], [], ["hub","chain drive"]],

	handlebars: ["tiller"],
	numberwheelsfront: 30,
	numberwheelsrear: 30,
},
},

{
object: {
	length: 210,
	width: 25,
	height: 80,
	weight: 12,
	image: "url('https://nederlandersfietsen.nl/content/uploads/challenge-seiran.jpeg')",
},
manufactured: {
	constructor: "Challenge",
	modele: "Seiran",
},
rolling: {
	wheels: [ "wheel?size=26", "wheel?size=26"],
	wheels_xyz: [ [0,0,0], [0,5,140] ],
	wheels_brake: [ ["disc"], ["disc"] ],
	wheels_steering: [ ["fork"] ],
	wheels_motor: [ ["hub"], ["chain drive"] ],

	handlebars: ["tiller"],
	numberwheelsfront: 20,
	numberwheelsrear: 20,
},
},
];FILES.data = [...FILES.data,...data_append];}
