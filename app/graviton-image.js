{const data_append = [
// FUNCTIONS
{	F				: true,
	n				: "openimagelayers",
	desc			: "open multiple image as layer (same position)|en",
	lang			: "es",
},
{	F				: true,
	n				: "moveimg",
	desc			: "move div who contains image incremental|en",
	input			: ["left","top"],
	lang			: "es",
},
{	F				: true,
	n				: "moveimgabs",
	desc			: "move div who contains image, absolute position|en",
	input			: ["ev"],
	lang			: "es",
},
{	F				: true,
	n				: "sizeimg",
	desc			: "change size of image|en",
	input			: ["addsize"],
	lang			: "es",
},
// TRAITS
{	T				: "project",
	readme			: {},
	lastcommit		: {},
},
{	T				: "viewinbrowser",
	html64			: {},
},


// DATA
{	identify			: { n	 	: "clickcoordinate" },
	callesfn			: {	n		: "=moveimgabs" },
	listenmouse			: {	mousedown	:  true, from : "domwindow" },
},
{	identify			: { n	 	: "biggersizeimg" },
	callesfn			: {	n		: "=sizeimg~0.05" },
	listenkeyboard		: {	keydown	: ["+"] },
	listenmouse			: {	click	:  true }, // test to desactivate
},

{	identify			: { n	 	: "smallersizeimg" },
	callesfn			: {	n		: "=sizeimg~-0.05" },
	listenkeyboard		: {	keydown	: ["-"] },
	listenmouse			: {	click	:  true },
},
{	identify			: { n	 	: "moveimgright" },
	callesfn			: {	n		: "=moveimg~10~0" },
	listenkeyboard		: {	keydown	: ["ArrowRight","d"] },
	listenmouse			: {	click	:  true },
},
{	identify			: { n	 	: "moveimgleft" },
	callesfn			: {	n		: "=moveimg~-10~0" },
	listenkeyboard		: {	keydown	: ["ArrowLeft","q"] },
	listenmouse			: {	click	: true },
},
{	identify			: { n	 	: "moveimgdown" },
	callesfn			: {	n		: "=moveimg~0~10" },
	listenkeyboard		: {	keydown	: ["ArrowDown","s"] },
	listenmouse			: {	click	: true },
},
{	identify			: { n	 	: "moveimgtop" },
	callesfn			: {	n		: "=moveimg~0~-10" },
	listenkeyboard		: {	keydown	: ["ArrowUp","z"] },
	listenmouse			: {	click	: true },
},

{	identify			: { n	 	: "moveimgrightbig" },
	callesfn			: {	n		: "=moveimg~50~0" },
	listenkeyboard		: {	keydown	: ["[ctrl]ArrowRight"] },
	listenmouse			: {	click	:  true },
},
{	identify			: { n	 	: "moveimgleftbig" },
	callesfn			: {	n		: "=moveimg~-50~0" },
	listenkeyboard		: {	keydown	: ["[ctrl]ArrowLeft"] },
	listenmouse			: {	click	: true },
},
{	identify			: { n	 	: "moveimgdownbig" },
	callesfn			: {	n		: "=moveimg~0~50" },
	listenkeyboard		: {	keydown	: ["[ctrl]ArrowDown"] },
	listenmouse			: {	click	: true },
},
{	identify			: { n	 	: "moveimgtopbig" },
	callesfn			: {	n		: "=moveimg~0~-50" },
	listenkeyboard		: {	keydown	: ["[ctrl]ArrowUp"] },
	listenmouse			: {	click	: true },
},



// STARTING
{	identify			: { n	 	: "openimagelayers" },
	callesfn			: {	n		: "=openimagelayers~5" },
	startfn				: true,
},

];FILES.app = [...FILES.app,...data_append];}
// TODO localstorage to clean : encodeutf 8 16, img0..4, imgname0..5

function sizeimg(addsize) {
//	document.getElementById('divimg').
//	if (debug) console.log('X :',ev.clientX,' Y: ',ev.clientX);
//console.log(addsize);
	const e = document.getElementById('terrain');
	if (e) {
		const val = parseFloat(e.style.fontSize.substr(0,e.style.fontSize.indexOf('rem')+1));
		e.style.fontSize = (val+addsize) + 'rem';
	}
}

function moveimgabs(ev) {
//	document.getElementById('divimg').
//	if (debug) console.log('X :',ev.clientX,' Y: ',ev.clientX);
	const e = document.getElementById('divimg');

	if (e) {
		e.style.left = (ev.clientX - 310) + 'px';
		e.style.top = (ev.clientY - 100) + 'px';
	}
}

function moveimg(left,top) {
//	document.getElementById('divimg').
//	if (debug) console.log('left :',left,' top: ',top);
	const e = document.getElementById('divimg');
	if (e) {
		if (e.style.left == '' || e.style.left == '0')
			e.style.left = left + 'px';
		else {
			const val = parseFloat(e.style.left.substr(0,e.style.left.indexOf('px')+1));
			e.style.left = (val+left) + 'px';
		}

		if (e.style.top == '' || e.style.top == '0')
			e.style.top = top + 'px';
		else {
			const val = parseFloat(e.style.top.substr(0,e.style.top.indexOf('px')+1));
			e.style.top = (val+top) + 'px';
		}
	}
//	if (debug) console.log('fin');
}

function openimagelayers(nblayer=1) {
//	createStyle('#divimg:hover > .myimageelement {background-color: red}');
	createStyle(`
	#divimg input[type=text] {
		border: none; font-size: 4em;width: 100%; top: 0.1em;font-weight: bold;background-color:rgba(125, 203, 214,0.8);color:white; border-radius: 15px;z-index: 11;
	}
	#divimg:hover > .control { z-index: 14; }
	`);
	const terrain = document.createElement('div');
	terrain.id = 'terrain';
	terrain.style.height = '30rem';
	terrain.style.fontSize = '0.4rem';
	const div = document.createElement('div');
	div.id = 'divimg';
	div.style.position = 'relative';
	div.style.height = '20em';
	div.style.width = '20em';
	const name = document.createElement('input');
	name.setAttribute('type','text');
	name.className = 'control';
	name.style.position = 'absolute';
	name.value = 'DuoQuest';
	div.appendChild(name);
	
	for (let i = 0; i < nblayer; i++) {
		const spanname = document.createElement('span');
		spanname.id = 'myimagename'+i;
		document.getElementById('choice').appendChild(spanname);
		const img = document.createElement('div');
		img.id = 'myimage'+i;
		img.className = 'myimageelement';
		img.style.position = 'absolute';
//		img.style.maxWidth = '40rem';
		img.style.backgroundSize = 'contain';
		img.style.backgroundRepeat = 'no-repeat';
//		img.style.left = '4rem';
//		img.style.top = '0.5rem';
		img.style.height = '100%';
		img.style.width = '100%';
		img.style.zIndex = (i+10)+'';
		if (i == 1)
			img.style.filter = 'drop-shadow(0px 20px 10px rgba(0,0,0,0.2))';
		else
			img.style.filter = 'drop-shadow(-10px -0px 10px rgba(0,0,0,0.4))';
		
		div.appendChild(img);
		const filech = document.createElement('input');
		filech.setAttribute('type','file');
		filech.id = 'fileinput'+i;
		filech.addEventListener('change',function(ev) {
//			if (debug) console.log('Event : ',ev);
			const file = ev.target.FILES[0];
//			console.log(file);
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = readerEvent => {
				const dataurl = readerEvent.target.result; // this is the content!
//				console.log(dataurl.length); //Always returns 2,000
				localStorage.setItem('img'+i, dataurl);
				localStorage.setItem('imgname'+i, file.name);
				const decodedurl = localStorage.getItem('img'+i);
//				console.log(decodedurl.length); //Always returns 2,000
				document.getElementById('myimage'+i).style.backgroundImage = 'url('+decodedurl+')';
				document.getElementById('myimagename'+i).style.textContent = file.name;
			}
		});
		document.getElementById('choice').appendChild(filech);
		const decodedurl = localStorage.getItem('img'+i);
		const imgname = localStorage.getItem('imgname'+i);
		img.style.backgroundImage = 'url('+decodedurl+')';
		spanname.textContent = imgname;

	}
	terrain.appendChild(div);
	document.getElementById('result').appendChild(terrain);
}

function test2() {
	let img = document.createElement('img');
	img.id = 'myimage';
	document.getElementById('result').appendChild(img);
	const filech = document.createElement('input');
	filech.setAttribute('type','file');
	filech.id = 'fileinput';
	filech.addEventListener('change',function(ev) {
		const file = ev.target.FILES[0];
		console.log(file);
    	let reader = new FileReader();
    	reader.readAsArrayBuffer(file);
		reader.onload = readerEvent => {
			var arrayBuff = readerEvent.target.result; // this is the content!


			console.log(arrayBuff.byteLength); //Always returns 2,000

			//Decode and encode same data without making any changes
			var decoded = String.fromCharCode(...new Uint8Array(arrayBuff));
			localStorage.setItem('encodeutf', decoded);
			let decoded2 = localStorage.getItem('encodeutf');
			var encoded = Uint8Array.from([...decoded].map(ch => ch.charCodeAt())).buffer;

			console.log(encoded.byteLength);

			document.getElementById('myimage').src = URL.createObjectURL(new Blob([decoded2], { type: 'image/png' } ));
		}
	});
	document.getElementById('menu').appendChild(filech);
}
