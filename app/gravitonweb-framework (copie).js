'use strict';

let files = {}; // original data files loaded

let g = {}; // global data vars from data files

let f = []; // function objects from data files
let fn = []; // function names from data files

let t = {}; // trait objects from data files
let p = []; // property + trait names from data files

let q = {}; // query objects from data files

let d = {}; // data objects preprocessed from data files
let h = []; // data objects preprocessed and optimized (same order than pn and pt)
let nbh = 0; // number of data elements

document.addEventListener('DOMContentLoaded', (event) => {
	initSettings('app',true);
	startfn();

	// Tests
//	const addlistener = true;
//	const trait = h[p.indexOf(':listenmouse')];
//	if (trait) {
//		const identity = h[p.indexOf('n:identify')];
//		const func = h[p.indexOf('-fn-n:callesfn')];
//		const click = h[p.indexOf('click:listenmouse')];
//		const from = h[p.indexOf('-ca-from:listenmouse')];
//		const textlabel = h[p.indexOf('-ca-textlabel:listenmouse')];
//		
//		const menu = document.createDocumentFragment();
//		let i = 0;
//		for (const hastrait of trait) {
//			if (hastrait) {
//				if (click[i]) {
//					const btn = document.createElement('button');
//					btn.id = from[i];
////					btn.className = 'square-button';
//					if (addlistener && func[i])
//						btn.addEventListener('click',func[i]);
//					const span = document.createElement('span');
//					span.className = 'txt-' + identity[i];
//					span.textContent = textlabel[i];
//					btn.appendChild(span);
//					menu.appendChild(btn);
//				}
//			}
//			i++;
//		}
//		document.getElementById('menu').appendChild(menu);
//	}
});

// UTILITY functions
// -----------------------------------------------------------
class MyError extends Error {
  constructor(message) {
    super(message);
    this.name = 'MyError';
  }
}
function isLetter(c) {
  return c.toLowerCase() != c.toUpperCase();
}
function isCharNumber(c) {
  return c >= '0' && c <= '9';
}
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function getNode(n, v) {
	n = document.createElementNS("http://www.w3.org/2000/svg", n);
	for (let p in v)
		n.setAttributeNS(null, p.replace(/[A-Z]/g, function(m, p, o, s) { return "-" + m.toLowerCase(); }), v[p]);
	return n;
}
function download(filename, text) {
	let pom = document.createElement('a');
	pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	pom.setAttribute('download', filename);
	pom.click();
}
function convertMarkdownHtml(markdown) {
	const htmlText = markdown
		.replace(/^# (.*$)/gim, '<h3>$1</h3>')
		.replace(/^## (.*$)/gim, '<h4>$1</h4>')
		.replace(/\*\*(.*)\*\*/gim, '<b>$1</b>')
		.replace(/\*(.*)\*/gim, '<i>$1</i>')
		.replace(/\-+ (.*)?/gim, '<ul><li>$1</li></ul>')
		.replace(/(\<\/ul\>\n(.*)\<ul\>*)+/gim,'')
		.replace(/\n$/gim, '<br />')
		.replace(/\[(.*?)\]\((.*?)\)/gim, "<a href='$2'>$1</a>");
	return htmlText;
}
function keynametotext(val) {
	switch (val) {
		case ' ':
			return 'Space';
		case 'ArrowLeft':
			return '←';
		case 'ArrowRight':
			return '→';
		default:
			return val;
	}
}


// BASE functions
// -----------------------------------------------------------
// Initial function loader
function startfn() {
	const trait = h[p.indexOf(':startfn')];
	if (trait) {
		const order = h[p.indexOf('-fn-n:order')];
		const func = h[p.indexOf('-fn-n:callesfn')];
		const para = h[p.indexOf('-in-n:callesfn')];

		let i = 0;
		let pipe = []; // TODO order of execution
		for (const hastrait of trait) {
			if (hastrait) {
				if (func[i])
					func[i](g,para[i]);
			}
			i++;
		}
	}
}
// Pre-process data with special information (trait, executions, query)
function preprocess(e,val,execute = true,FNDELIM='~') {
	let out = [val];
	const val2 = val.substr(1);
	const vstart = val.charAt(0);
	switch (vstart) {
		case "=":
			if (val2.includes('e.') && !val2.includes(FNDELIM)) {
				const paramfn = new Function('e','const x='+val2+';return x;');
				out.push(execute ? paramfn(e) : undefined);
				out.push(paramfn);
				if (val2.includes('e.')) {
					const tmppar1 = val2.substr(val2.indexOf('e.'));
					let tmppar2 = '';
					let i = 0;
					while (isLetter(tmppar1.charAt(i)) || tmppar1.charAt(i) == '.') {
						tmppar2 += tmppar1.charAt(i);
						i++;
					}
					out.push(tmppar2);
				}
//				console.log("function with e",out);
			}
			else if (!val2.includes('e.') && !val2.includes(FNDELIM) && ( val2.startsWith("'") || val2.startsWith("(") || isCharNumber(val2.charAt(0)))) {
				const paramfn = new Function('const x='+val2+';return x;');
				out.push(execute ? paramfn() : undefined);
				out.push(paramfn);
//				console.log("function without e",out);
			}
			else {
				const separator = val2.indexOf(FNDELIM);
				if (separator != -1) {
					const fnname = val2.substr(0,separator);
					const tmpfn = f[fn.indexOf(fnname)];;
					out.push(execute ? tmpfn(val2.substr(separator+1)) : undefined);
					out.push(tmpfn);
					out.push(val2.substr(separator+1));
				}
				else {
					const ifn = fn.indexOf(val2);
					out.push(execute && ifn != -1 ? f[ifn]() : undefined);
					out.push(f[ifn]);
				}
			}
			break;
		case ":":
			break;
		case "?":
			break;
		default:
			break;
	}
	return out;
}

// Load local settings from database for fast access
function initSettings(setting, viewlog=false) {
	const FNDELIM = '~';
	const dataall = files[setting];
    const arrgen = (len) => Array.from({length: len});
    const arrgen20 = (len) => Array.from({length: len}, function(v,i) {
    	return [];
    });
    const arrgen2l = (len, len2) => Array.from({length: len}, function(v,i) {
    	const a = (len) => Array.from({length: len});
    	return a(len2);
    });
    
    // initialize temp data view
    let tmpdata = { F : [], T : [], Q : [], D : [], G : []};
	for (const e of dataall) {
		if (e.hasOwnProperty('F'))		tmpdata.F.push(e);
		else if (e.hasOwnProperty('T'))	tmpdata.T.push(e);
		else if (e.hasOwnProperty('Q'))	tmpdata.Q.push(e);
		else if (e.hasOwnProperty('G'))	tmpdata.G.push(e);
		else							{ tmpdata.D.push(e); nbh++ }
	}

	// prepare FUNCTION from data
	for (const e of tmpdata.G) {
		for (const [key, val] of Object.entries(e)) {
			if (key != 'G')
				g[key] = val;
		}
	}
	if (viewlog) console.log('g:',g);

	// prepare FUNCTION from data
	for (const e of tmpdata.F) {
		const fnname = e.n;
		fn.push(fnname);
		
		// load inline code
		if (e.code) {
			let tmpinput = [];
			let tmpcode = e.code;
			let tmpheader = '';
			let tmpfooter= '';
//			let tmpfooter = '\nthrow new MyError("'+fnname+':");';
			if (e.input) {
				let i = 0;
				for (const val of e.input) {
					// if param name include reference to entity object, modify prototype and var names
					if (val.includes('.')) {
						tmpcode = tmpcode.replaceAll(val,'var' + i);
						const tmpval = val.substr(0,val.indexOf('.'));

						if (tmpinput.includes(tmpval)) {
							tmpinput.push(tmpval+i);
							tmpheader += 'const var'+i+'=((typeof '+tmpval+i+'==="undefined" && typeof '+tmpval+'==="object")||typeof '+tmpval+i+'==="object")?'+val+':'+tmpval+i+';';
						}
						else {
							tmpinput.push(tmpval);
							tmpheader += 'const var'+i+'=(typeof '+tmpval+'==="object")?'+val+':'+tmpval+';';
						}
					}
					else tmpinput.push(val);
					i++;
				}
			}
//				console.log(tmpinput,"new function",tmpinput.join(','),tmpheader + tmpcode);
			const tmpfn = new Function('g,'+tmpinput.join(','),tmpheader + tmpcode + tmpfooter);
			f.push(tmpfn);
//				fn.push(nameFunction(tmpfn, fnname));
			
		}
		// load external code
		else
			if (typeof window[fnname] === 'function') f.push(window[fnname]); else f.push();
	}
	if (viewlog) console.log('fn:',fn,f);
	
	// load TRAIT type
	const STEP = 4;
	let x = 0;
	for (const e of tmpdata.T) {
		const name = e.T;
		t[name] = e;
		delete t[name].T;
		t[name]['index'] = x;
		x++;
		for (const [prop, val] of Object.entries(e)) {
			if (typeof t[name][prop] !== 'number') {
				t[name][prop]['index'] = x;
				x += STEP;
			}
			if (val.hasOwnProperty('defaultvalue')) {
				if (typeof val.defaultvalue === 'string') {
					if (val.defaultvalue.startsWith('=')) {
						if (!val.defaultvalue.startsWith('=e.') && !val.defaultvalue.startsWith("='") && !val.defaultvalue.startsWith("=(") && !isCharNumber(val.defaultvalue.charAt(1))) {
							const fullname = val.defaultvalue.substr(1);
							const separator = fullname.indexOf(FNDELIM);
							if (separator != -1) {								
								const fnname = fullname.substr(0,separator);
								val['defaultvaluefn'] = f[fn.indexOf(fnname)];;
								val['defaultvalueparam'] = fullname.substr(separator+1);
							}
							else
								val['defaultvaluefn'] = f[fn.indexOf(fullname)];
						}
						else if (!val.defaultvalue.includes('e.')) {
							const paramfn = new Function('e','const value='+val.defaultvalue.substr(1)+';return (typeof value === "string") ? value : value;');
							val['defaultvalue'] = paramfn();
						}
					}
				}
			}
		}
	}
	if (viewlog) console.log('t:',t);

	// Generate list of properties with index correspondance
	const tmpp = arrgen(x); // property name + trait name
	for (const [key,val] of Object.entries(t)) {

		tmpp[val.index] = ':'+key
		for (const [k,v] of  Object.entries(val)) {
			if (k != 'index') {
				tmpp[v.index] = k+':'+key;
				tmpp[v.index+1] = '-ca-'+k+':'+key;
				tmpp[v.index+2] = '-fn-'+k+':'+key;
				tmpp[v.index+3] = '-in-'+k+':'+key;
			}
		}
	}

	// load normal data type
	const tmph = arrgen20(x);
	let id = 0;
	for (const e of tmpdata.D) {
		d[id] = {};
		for (const [traitname, trait] of Object.entries(t)) {
			if (!e.hasOwnProperty(traitname)) {
				tmph[trait.index].push(false);
				for (const [key, val] of Object.entries(trait)) {
					if (key != 'index') {
						tmph[val.index].push(undefined);
						tmph[val.index+1].push(undefined);
						tmph[val.index+2].push(undefined);
						tmph[val.index+3].push(undefined);
					}
				}
			}
			else {
				for (const [etraitname, etrait] of Object.entries(e)) {
					d[id][etraitname] = {};
					if (t[etraitname]) {
						for (const [prop, tval] of Object.entries(t[etraitname])) {
							if (prop != 'index') {
								if (tval.hasOwnProperty('defaultvalue') && (typeof etrait[prop] === 'undefined')) {
									if (typeof tval.defaultvalue === 'string') {
										const pp = preprocess(e,tval.defaultvalue);
										d[id][etraitname][prop] = pp[0];
										d[id][etraitname]['-ca-'+prop] = pp[1];
										d[id][etraitname]['-fn-'+prop] = pp[2];
										d[id][etraitname]['-in-'+prop] = pp[3];
									}
									else d[id][etraitname][prop] = tval.defaultvalue;
								}
								else {
									const epropval = etrait[prop];
									if (typeof epropval === 'string') {
										if (tval.hasOwnProperty('execute')) {
											const pp = preprocess(e,epropval,tval.execute);
											d[id][etraitname][prop] = pp[0];
											d[id][etraitname]['-fn-'+prop] = pp[2];
											d[id][etraitname]['-in-'+prop] = pp[3];
										}
										else {
											const pp = preprocess(e,epropval);
											d[id][etraitname][prop] = pp[0];
											d[id][etraitname]['-ca-'+prop] = pp[1];
											d[id][etraitname]['-fn-'+prop] = pp[2];
											d[id][etraitname]['-in-'+prop] = pp[3];
										}
									}
									else if (Array.isArray(epropval)) {
										const tmp = [];
										for (const v of epropval) {
											if (typeof v === 'number')
												tmp.push(v);
											else if (typeof v === 'string') {
												tmp.push(preprocess(e,v)[0]);
											}
										}
										d[id][etraitname][prop] = tmp;
		//								console.log("test:",epropval);
									}
									else d[id][etraitname][prop] = epropval;
								}
							}
						}
					}
				}
				for (const [key, val] of Object.entries(trait)) {
					if (key != 'index') {
						tmph[val.index].push(d[id][traitname][key]);
						if (d[id][traitname][key]) {
							if ((typeof d[id][traitname][key] === 'string') && d[id][traitname][key].charAt(0) == '=') {
								if (d[id][traitname].hasOwnProperty('-ca-'+key))
									tmph[val.index+1].push(d[id][traitname]['-ca-'+key]);
								else
									tmph[val.index+1].push(undefined);

								if (d[id][traitname].hasOwnProperty('-fn-'+key))
									tmph[val.index+2].push(d[id][traitname]['-fn-'+key]);
								else
									tmph[val.index+2].push(undefined);

								if (d[id][traitname].hasOwnProperty('-in-'+key))
									tmph[val.index+3].push(d[id][traitname]['-in-'+key]);
								else
									tmph[val.index+3].push(undefined);
							}
							else {
								tmph[val.index+1].push(undefined);
								tmph[val.index+2].push(undefined);
								tmph[val.index+3].push(undefined);
							}
						}
						else {
							tmph[val.index+1].push(undefined);
							tmph[val.index+2].push(undefined);
							tmph[val.index+3].push(undefined);
						}
					}
				}
				tmph[trait.index].push(true);
			}
		}
		id++;
	}
	if (viewlog) console.log('d:',d);
//	if (viewlog) console.log('orig p:',p,h);
	
	// remove lines with no data :
	h = [...tmph];
	p = [...tmpp];
	const len = tmph.length;
	let nbremove = 0;
	for (let i = 0; i < len ; i++) {
		const val = tmph[i];
		let j = val.length;
		while (typeof val[j-1] === 'undefined' && j > 0)
			j--;

		if (j == 0) {
			h.splice(i - nbremove,1);
			p.splice(i - nbremove,1);
			nbremove++;
		}
	}
	if (viewlog) console.log('new h', p,h);
}

function keydownHandle(ev) {
	let found = false;
	const trait = h[p.indexOf(':listenkeyboard')];
	const keyup = h[p.indexOf('keyup:listenkeyboard')];
	const keydown = h[p.indexOf('keydown:listenkeyboard')];
	const func = h[p.indexOf('-fn-n:callesfn')];
	const para = h[p.indexOf('-in-n:callesfn')];
	const nbcall = h[p.indexOf('nbcall:listenkeyboard')];

	let i = 0;
	g.lastkey = ev.key;
	for (const hastrait of trait) {

		if (hastrait ?? keydown[i]) {
			for (const val of keydown) {
				if (val === undefined || val.length == 0) {
					if (keyup[i] !== undefined && keyup[i].length != 0) {
						for (const k of keyup[i])
							if (ev.key === k && g.keyscactive) {
								ev.preventDefault();
								found = true;
							}
					}
				}
				else {
					for (const k of val) {
						if (k.length == 1) {
							if (!ev.ctrlKey && ev.key === k && g.keyscactive) {
								ev.preventDefault();
								func[i](g,para[i]);
								nbcall[i]++;
								found = true;
							}
						}
						else if (k.startsWith('[')) {
							if (k.substr(1,k.indexOf(']') - 1) == 'ctrl') {
								if (ev.ctrlKey && ev.key === k.substr(k.indexOf(']') + 1)) {
									ev.preventDefault();
									func[i](g,para[i]);
									nbcall[i]++;
									found = true;
								}
							}
						}
						else {
							if (!ev.ctrlKey && ev.key === k) {
								ev.preventDefault();
								func[i](g,para[i]);
								nbcall[i]++;
								found = true;
							}
						}
					}
				}
				if (found) break;
			}
			if (found) break;
		}
		i++;
	}

}

function keyupHandle(ev) {
	let found = false;
	const trait = h[p.indexOf(':listenkeyboard')];
	const keyup = h[p.indexOf('keyup:listenkeyboard')];
	const func = h[p.indexOf('-fn-n:callesfn')];
	const para = h[p.indexOf('-in-n:callesfn')];
	const nbcall = h[p.indexOf('nbcall:listenkeyboard')];

	let i = 0;
	for (const hastrait of trait) {
		const key = keyup[i];
		if (hastrait && key) {
//console.log(hastrait,key,' ',hastrait && key, hastrait ?? key)
			for (const k of key) {
				if (k.length == 1) {
					if (!ev.ctrlKey && ev.key === k && g.keyscactive ) {
						func[i](g,para[i]);
						nbcall[i]++;
						found = true;
					}
				}
				else if (k.startsWith('[')) {
					if (k.substr(1,k.indexOf(']') - 1) == 'ctrl') {
						if (ev.ctrlKey && ev.key === k.substr(k.indexOf(']') + 1)) {
							func[i](g,para[i]);
							nbcall[i]++;
							found = true;
						}
					}
				}
				else {
					if (!ev.ctrlKey && ev.key === k) {
						func[i](g,para[i]);
						nbcall[i]++;
						found = true;
					}
				}
				if (found) break;
			}
			if (found) break;
		}
		i++;
	}
}



	// Create shortcuts help in footer
//	const shortcut = document.getElementById('shortcuts');
//	shortcut.textContent = '';
//	shortcut.appendChild(createFnHelp());
//	
//	// Create checkbox for options and update state of checkbox with new setting
//	const frag = document.createDocumentFragment();
//	for (const i in fn_change) {
//		if (fn_change[i]) {
//			const input = document.createElement('input');
//			input.setAttribute('type','checkbox');
//			input.id = fn_name[i].join('-');
//			input.checked = fn_bool[i];
//			frag.appendChild(input);
//		}
//	}
//	document.getElementById('global').insertBefore(frag,document.getElementById('menu')); 


//// Update database settings from local settings
//function updateSettings(setting) {
//	const dsetting = d[setting];
////	dsetting['fn'] = fn;
//	dsetting['fn_name'] = fn_name;
//	dsetting['fn_params'] = fn_params;
//	dsetting['fn_click'] = fn_click;
//	dsetting['fn_keyup'] = fn_keyup;
//	dsetting['fn_keydown'] = fn_keydown;
//	dsetting['fn_change'] = fn_change;
//	dsetting['fn_nbcall'] = fn_nbcall;
//	dsetting['fn_bool'] = fn_bool;
//	dsetting['fn_num'] = fn_num;
//	dsetting['fn_str'] = fn_str;
//	dsetting['fn_enabled'] = fn_enabled;

//	// Create shortcuts help in footer
//	const shortcut = document.getElementById('shortcuts');
//	shortcut.textContent = '';
//	shortcut.appendChild(createFnHelp());
//}

//// sort functions call from shortcut usage
//function sortfnbyusage() {
//	const LEN = fn_nbcall.length;
//    const arrgen = (len) => Array.from({length: len});

//  	const index = arrgen(LEN);
//	for (let i = 0; i < LEN ; i++) index[i] = [fn_nbcall[i],i];
//	index.sort(function(a, b){return b[0] - a[0]});

//	const fonc = arrgen(LEN);
//	const name = arrgen(LEN);
//	const namefull = arrgen(LEN);
//	const params = arrgen(LEN);
//	const click = arrgen(LEN);
//	const keyup = arrgen(LEN);
//	const keydown = arrgen(LEN);
//	const change = arrgen(LEN);
//	const nbcall = arrgen(LEN);
//	const bool = arrgen(LEN);
//	const num = arrgen(LEN);
//	const str = arrgen(LEN);
//	const enabled = arrgen(LEN);

//	for (let i = 0; i < LEN ; i++) {
//		const x = index[i][1];
//		fonc[i] = fn[x];
//		name[i] = fn_name[x];
//		namefull[i] = fn_name[x].join('');
//		params[i] = fn_params[x];
//		click[i] = fn_click[x];
//		keyup[i] = fn_keyup[x];
//		keydown[i] = fn_keydown[x];
//		change[i] = fn_change[x];
//		nbcall[i] = fn_nbcall[x];
//		bool[i] = fn_bool[x];
//		num[i] = fn_num[x];
//		str[i] = fn_str[x];
//		enabled[i] = fn_enabled[x];
//	}
//	fn = fonc;
//	fn_name = name;
//	fn_namefull = namefull;
//	fn_params = params;
//	fn_click = click;
//	fn_keyup = keyup;
//	fn_keydown = keydown;
//	fn_change = change;
//	fn_nbcall = nbcall;
//	fn_bool = bool;
//	fn_num = num;
//	fn_str = str;
//	fn_enabled = enabled;
//}

//function appendProjectInformationsCSS () {
//	if (lang != "en") {
//		script = document.createElement('script');
//		script.setAttribute('src','README_'+lang+'.js');
//		document.getElementsByTagName('head')[0].appendChild(script);
//	}
//	document.getElementById('readme').innerHTML = convertMarkdownHtml(readme_str);
//	const datelastact = new Date(lastcommit['date']);
//	document.getElementById('project-lastactivity').innerHTML = '<ul><li>' + datelastact.toDateString() + ' ' + datelastact.toLocaleTimeString()+'</li><li>'+lastcommit['description']+'</li></ul>';
//}
//function appendReadme() {
//	const htmlText = convertMarkdownHtml(readme_str);
//	document.getElementById('readme').innerHTML = htmlText.trim();
//}

function addClickListener() {
	let i = 0;
	for (const val of fn_click) {
		if (val) {
			const elem = document.getElementById(fn_name[i].join(''));
			if (elem !== null) {
				elem.addEventListener('click', fn[i]);
				if (fn_name[i].length == 2) elem.fn_name = fn_name[i][1];
				elem.fn_params = fn_params[i];
			}
		}
		i++;
	}
}

function addChangeListener() {
	let i = 0;
	for (const val of fn_change) {
		if (val) {
			const elem = document.getElementById(fn_name[i].join(''));
			if (elem !== null) {
				elem.addEventListener('change', fn[i]);
				if (fn_name[i].length == 2) elem.fn_name = fn_name[i][1];
				elem.fn_params = fn_params[i];
			}
		}
		i++;
	}
}

function initCount() {
	validcount = 0;
	uncertaincount = 0;
	invalidcount = 0;
	pagecount = 0;
	selectcount = 0; //TODO
}
function updateCount() {
	document.getElementById('selectcount').textContent = '';
	if (validcount == 0)
		document.getElementById('selectcount').innerHTML += '<span class="text txt-select">0 / '+(invalidcount)+' </span>';
	else if (validcount == 1)
		document.getElementById('selectcount').innerHTML += '<span class="text txt-select">1 / '+(validcount+invalidcount)+' </span>';
	else if (validcount > 1)
		document.getElementById('selectcount').innerHTML += '<span class="text txt-select-2">'+validcount+' / '+(validcount+invalidcount)+' </span>';
	

	if (uncertaincount == 1)
		document.getElementById('selectcount').innerHTML += ' [<span class="text txt-data">'+uncertaincount+' </span> <span class="txt-notknown"></span>]';
	else if (uncertaincount > 1)
		document.getElementById('selectcount').innerHTML += ' [<span class="text txt-data-2">'+uncertaincount+' </span> <span class="txt-notknown-2"></span>]';
	document.getElementById('selectcount').innerHTML += ', total ' + d.length;

}

function loadVars(name,dataorigin,dataoriginname) {
	// Retrieve variable names loaded with script in HTML header
	if (name != '' && typeof name !== 'undefined') {
		const prefix = name+'_' ;
		const loaded = Object.keys(window).filter(e => e.startsWith(prefix));
		if (dataoriginname != '' && typeof dataoriginname !== 'undefined') {
			loaded.forEach(function(name) {
				dataorigin = Object.assign(dataorigin,window[name]);
				dataoriginname[name.slice(prefix.length)] = window[name];
			});
		}
		else {
			loaded.forEach(function(name) {
				dataorigin[name.slice(prefix.length)] = window[name];
			});
		}
	}
}

function loadLang(lang) {
	if (lang) {
		// loading language text for definitions
		const link1 = document.createElement('link');
		link1.setAttribute('rel','stylesheet');
		link1.setAttribute('href','lang/'+lang+'/def_'+lang+'.css');
		document.getElementsByTagName('head')[0].appendChild(link1);

		// loading language text for user interface
		const link2 = document.createElement('link');
		link2.setAttribute('rel','stylesheet');
		link2.setAttribute('href','lang/'+lang+'/gravitonweb_'+lang+'.css');
		document.getElementsByTagName('head')[0].appendChild(link2);

		// lang marker in interface
		document.getElementById('lang'+lang).classList.add('langactive');
	}
}

function editor() { //TODO
	if (document.getElementById('option-editor').checked) {
		for (const [prop, val] of Object.entries(d[document.getElementById('sliderpageid').value])) {
//			console.log(prop);
//			console.log(p[prop]);
			if (typeof p[prop] !== 'undefined') {
				switch (p[prop]['type']) {
					case 'range':
					case 'text':
					case 'imageurl':
						document.getElementById('edit-'+prop).value = val+'';
						break;
					case 'link':
						document.getElementById('edit-'+prop).value = val['url']; //TODO data label ?
					break;
				}
			}
		}
	}

}

// --------------------------------------------------------------------------------
// ACTIONS
function scroll(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	if (name !== undefined && name != '') {
		document.getElementById('sliderpageid').value = document.getElementById('val-scroll'+name).value;
		evaluate();
	}
}
function option(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	if (name !== undefined && name != '') {
//	console.log(evt);
		if (evt.type != 'change') {
			const checkval = !document.getElementById('option-' + name).checked;
			document.getElementById('option-' + name).checked = checkval;
			document.getElementById('footer-option-' + name).checked = checkval;
			fn_bool[fn_namefull.indexOf('option'+name)] = checkval;
		}
		else {
			document.getElementById('footer-' + evt.target.id).checked = evt.target.checked;
			fn_bool[fn_namefull.indexOf('option'+name)] = evt.target.checked;
		}
		const fnname = 'option' + name ; //TODO remove 'option' hardcoded
		if (typeof window[fnname] === 'function') {
			const fnoption = window[fnname];
			fnoption();
		}
		evaluate();
//		console.log('option'+name,fn_namefull,fn_bool);
	}
}
function exportdatatofile(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	if (name === undefined || name == '')
		download('data_1.js','{const data_append =' + JSON.stringify(d) + ';d = Object.assign(d,data_append);f["1"] = data_append;}');
	else
		download('data_'+name+'.js','{const data_append =' + JSON.stringify(f[name]) + ';d = Object.assign(d,data_append);f["'+name+'"] = data_append;}');
}

// Save database to localstorage (navigator built-in storage)
function exportdatatolocal(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	sortfnbyusage();
	updateSettings('setting0');
	const heure = new Date();
	if (name === undefined || name == '') {
		localStorage.setItem('d', JSON.stringify(d));
		document.getElementById('event').innerHTML = 'All data saved at <span class="timestamp">'+heure.toTimeString()+' in local storage</span>';
	}
	else {
		localStorage.setItem(name, JSON.stringify(f[name]));
		document.getElementById('event').innerHTML = 'Data['+ name +'] saved at <span class="timestamp">'+heure.toTimeString()+' in localstorage</span>';
	}
}

// Update database from localstorage (navigator built-in storage)
function loaddatafromlocal(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	let exit = false;
	if (name === undefined || name == '') {
		if (JSON.parse(localStorage.getItem('d') === null)) {
			document.getElementById('event').innerHTML = 'No data to load in local storage</span>';
			exit = true;
		}
	}
	else {
		if (JSON.parse(localStorage.getItem(name) === null)) {
			document.getElementById('event').innerHTML = 'No data['+ name +'] to load in local storage</span>';
			exit = true;
		}
	}
	if (!exit) {
		if (name === undefined || name == '') d = JSON.parse(localStorage.getItem('d'));
		else f[name] = JSON.parse(localStorage.getItem(name));

		if (name != 'application') {
			document.getElementById('result').textContent = '';
			document.getElementById('choice').textContent = '';
			document.getElementById('select').textContent = '';
			build();
		}
		initSettings('setting0');
		addKeyboardListener();
		addClickListener();
		addChangeListener();
		document.getElementById('event').innerHTML = ((name === undefined || name == '') ? 'All data' : 'Data['+ name +']') + ' loaded from local storage</span>';
	}
}

// Clean localstorage
function cleanlocaldata(evt) {
	const name = (evt !== undefined && evt.target) ? evt.target.fn_name : evt;
	if (name === undefined || name == '') {
		localStorage.removeItem('d');
		document.getElementById('event').innerHTML = 'Global data cleaned from local storage !</span>';
	}
	else {
		localStorage.removeItem(name);
		document.getElementById('event').innerHTML = 'Data['+ name +'] cleaned from local storage !</span>';
	}
}

function reload(evt) {
	document.getElementById('choice').textContent = '';
	document.getElementById('result').textContent = '';
	build();
	const heure = new Date();
	document.getElementById('event').innerHTML = 'data reloaded at <span class="timestamp">'+heure.toTimeString()+'</span>';
}
// --------------------------------------------------------------------------------
// CREATION
function createProjectInformationsJS() {
//		// Display previous loaded README file 
//		if (localStorage.getItem('readme_str') != null) {
//			const htmlText_session = convertMarkdownHtml(localStorage.getItem('readme_str'));
//			document.getElementById('readme').innerHTML = htmlText_session.trim();
//		}
//		else {
//			if (lang == "en")
//				appendReadme();
//			else
//				$('<script>').appendTo('head')
//				.attr({src: 'README_'+lang+'.js'});
//		}
//		localStorage.setItem('readme_str',readme_str);
	if (typeof readme_str !== "undefined")
		appendReadme();
	if (typeof lastcommit !== "undefined") {
		const datelastact = new Date(lastcommit['date']);
		document.getElementById('project-lastactivity').innerHTML = '<ul><li>' + datelastact.toDateString() + ' ' + datelastact.toLocaleTimeString()+'</li><li>'+lastcommit['description']+'</li></ul>';
	}
}
//				urlreadme = 'https://graviton.frama.io/graviton-web/README_'+lang+'.md';
//		fetch(urlreadme)
//			.then(response => response.text())
//			.then((readmedata) => {
//				const htmlText = convertMarkdownHtml(readmedata);
//				document.getElementById('readme').innerHTML = htmlText.trim();
//			localStorage.setItem('readme_str', readmedata);
////			console.log(data)
//		});
//	async function generateConversationWith(userId) {
//		const response = await fetch(`${process.env.API_URL}/graphql`, {
//		method: "POST",
//		credentials: "include",
//		headers: { "Content-Type": "application/json" },
//		body: JSON.stringify({
//			query: `
//			  mutation GenerateChatConversationWith {
//			    generateChatConversationWith(chatWithUserId: "userId") {
//			      id
//			    }
//			  }`,
//			}),
//		});
//		const responseData = await response.json();

//		console.log("response", responseData);
//	}

function createFnHelp() {
	const dl = document.createElement('dl');
	let i = 0;
	let x = 0;
	for (const val of fn_name) {
		x = 0;
		const dt = document.createElement('dt');
		const spantitle = document.createElement('label');
		spantitle.textContent = val.join(' : ') + ' ';
		spantitle.addEventListener('click', fn[i]);
		spantitle.fn_name = fn_name[i][1];
		spantitle.fn_params = fn_params[i];
		dt.appendChild(spantitle);
		if (fn_click[i]) {
			const spanclick = document.createElement('span');
			spanclick.className = 'key';
			spanclick.textContent = 'click';
			dt.appendChild(spanclick);
		}
		if (fn_change[i]) {
			const inputchange = document.createElement('input');
			inputchange.id = 'footer-' + val.join('-')
			inputchange.setAttribute('type','checkbox');
			inputchange.addEventListener('change',function(evt) {
				document.getElementById(evt.target.id.substr(7)).checked = evt.target.checked;
				evaluate();
			});
			inputchange.checked = fn_bool[i];
//			inputchange.className = 'option';
//			labelchange.textContent = 'change';
			dt.appendChild(inputchange);
		}
		
		dl.appendChild(dt);

		if (fn_keyup[i] !== undefined && fn_keyup[i].length > 0) {
			const dd = document.createElement('dd');
			for (const k of fn_keyup[i]) {
				if (x > 0) dd.appendChild(document.createTextNode(' or '));
				if (k.startsWith('[')) {
					const span1 = document.createElement('span');
					span1.className = 'key';
					span1.textContent = k.substr(1,k.indexOf(']') - 1);
					dd.appendChild(span1);
					dd.appendChild(document.createTextNode(' + '));
					const span2 = document.createElement('span');
					span2.className = 'key';
					span2.textContent = keynametotext(k.substr(k.indexOf(']') + 1));
					dd.appendChild(span2);
				}
				else {
					const span = document.createElement('span');
					span.className = 'key';
					span.textContent = keynametotext(k);
					dd.appendChild(span);
				}
				x++;
			}
			dl.appendChild(dd);
		}
		else if (fn_keydown[i] !== undefined && fn_keydown[i].length > 0) {
			const dd = document.createElement('dd');
			for (const k of fn_keydown[i]) {
				if (x > 0) dd.appendChild(document.createTextNode(' or '));
				if (k.startsWith('[')) {
					const span1 = document.createElement('span');
					span1.className = 'key';
					span1.textContent = k.substr(1,k.indexOf(']') - 1);
					dd.appendChild(span1);
					dd.appendChild(document.createTextNode(' + '));
					const span2 = document.createElement('span');
					span2.className = 'key';
					span2.textContent = keynametotext(k.substr(k.indexOf(']') + 1));
					dd.appendChild(span2);
				}
				else {
					const span = document.createElement('span');
					span.className = 'key';
					span.textContent = keynametotext(k);
					dd.appendChild(span);
				}
				x++;
			}
			dl.appendChild(dd);
		}
		i++;
	}
	return dl;
}
////let svg = getNode("svg");
//function createGraph(cat, key, svg) {
////		let gr = getNode('polyline', { stroke: 'orange', strokeWidth: 5, fill: 'pink', points: '50 160 55 180 70 180 60 190 65 205 50 195 35 205 40 190 30 180 45 180' });
////		svg.appendChild(gr);
////		var r = getNode('rect', { x: 10, y: 10, width: 100, height: 20, fill:'#ff00ff' });
////		svg.appendChild(r);
////		var r = getNode('rect', { x: 20, y: 40, width: 100, height: 40, rx: 8, ry: 8, fill: 'pink', stroke:'purple', strokeWidth:7 });
////		svg.appendChild(r);
//	document.getElementById('').appendChild(svg);
//}

function createText(def) {
	const divfilter = document.createElement('div');
	divfilter.id = def+'Filter';
	divfilter.className = 'property-filter';
	const divinfo = document.createElement('div');
	divinfo.id = def+'InfoBox';
	divinfo.className = 'property-info';
	divfilter.appendChild(divinfo);
	const inputview = document.createElement('input');
	inputview.id = 'txt-'+def;
	inputview.setAttribute('type','text');
	inputview.addEventListener('input',function() {
		const inputval = document.getElementById('txt-'+def).value;
		p[def]['textfilter'] = inputval;
		if (inputval !== undefined && inputval != '')
			p[def]['changed'] = true;
		else
			p[def]['changed'] = false;
		evaluate();
	});
	inputview.addEventListener('focusin', (ev) => {
//  			ev.target.style.background = 'pink';
		lettershortcuts = false;
//		console.log("focusin",lettershortcuts);
	});
	inputview.addEventListener('focusout', (ev) => {
//  			ev.target.style.background = 'pink';
		lettershortcuts = true;
	});

	divinfo.appendChild(inputview);
	const inputedit = document.createElement('input');
	inputedit.id = 'edit-'+def;
	inputedit.className = 'editor-input';
	inputedit.setAttribute('type','text');
	inputedit.addEventListener('focusin', (ev) => {
//  			ev.target.style.background = 'pink';
		lettershortcuts = false;
//		console.log("focusin",lettershortcuts);
	});
	inputedit.addEventListener('focusout', (ev) => {
//  			ev.target.style.background = 'pink';
		lettershortcuts = true;
	});
	divinfo.appendChild(inputedit);

	return divfilter;
}

function createCheckbox(def,style) {
	const div = document.createElement('div');
	div.id = def+'Boxes';
	div.className = 'property-boxes';

	p[def]['values'].forEach(function(key, index) {
		const checkId = def+'-'+key;
		document.documentElement.style.setProperty('--txt-'+def+'-'+key, '"'+key+'"');
		style.textContent += '.txt-' + def + '-' + key + ':after {content:var(--txt-' + def + '-' + key + ');} ';	
		const checkbox = document.createElement('input');
		checkbox.id = checkId;
		checkbox.setAttribute('type','checkbox');
		checkbox.className = def+'Check';
		checkbox.addEventListener('change',function () {
			if (
				p[def]['type'] == 'radio'
				&& this.checked
			) {
				document.getElementById(def+'Boxes').querySelector('input').checked = false;
				this.checked = true;
			}
			if (document.querySelectorAll('.'+def+'Check:checked').length == 0) {
				p[def]['changed'] = false;
			} else {
				p[def]['changed'] = true;
			}
			evaluate();
		});
		const label = document.createElement('label');
		label.setAttribute('for',checkId);
		label.className = 'property-box text txt-'+def+'-'+key
		div.appendChild(checkbox);
		div.appendChild(label);
	});
	return div;
}

function createSlider(def) {
	const out = document.createDocumentFragment();
	const stepdata = (p[def]['step'] !== null && p[def]['step'] !== undefined) ? p[def]['step'] : 0.1;

//		console.log(p[def]);
	// No data
	if (p[def]['min'] == 9999 && p[def]['max'] == 0) {
		const divfilter = document.createElement('div');
		divfilter.id = def+'Filter';
		divfilter.className = 'property-filter';
		const divinfo = document.createElement('div');
		divinfo.id = def+'InfoBox';
		divinfo.className = 'property-info txt-notknown';
		divfilter.appendChild(divinfo);

		out.appendChild(divfilter);
	}
	// only one data, view as checkbox
	else if (p[def]['min'] == p[def]['max']) {
		const key = "onevalue";
		const checkId = def+'-'+key;

		const divboxes = document.createElement('div');
		divboxes.id = def+'Boxes';
		divboxes.className = 'property-boxes';
		const inputnum = document.createElement('input');
		inputnum.id = 'min-'+def;
		inputnum.setAttribute('type','number');
		inputnum.setAttribute('style','display:none;');
		inputnum.setAttribute('min',p[def]['min']);
		inputnum.setAttribute('max',p[def]['max']);
		inputnum.setAttribute('step','any');
		divboxes.appendChild(inputnum);
		const inputche = document.createElement('input');
		inputche.id = checkId;
		inputche.setAttribute('type','checkbox');
		inputche.addEventListener('change', function () {
			if (!p[def]['changed'])
				p[def]['currMin'] = document.getElementById('min-'+def).value;
			p[def]['changed'] = !p[def]['changed'];
			evaluate();
		});
		divboxes.appendChild(inputche);
		const label = document.createElement('label');
		label.className = 'property-box text';
		label.setAttribute('for',checkId);
		label.textContent = p[def]['min']+((p[def]['unit'] !== undefined) ? ' '+p[def]['unit'] : '');
		divboxes.appendChild(label);

		out.appendChild(divboxes)
	}
	else {
		const divfilter = document.createElement('div');
		divfilter.id = def+'Filter';
		divfilter.className = 'property-filter';


		// Box with numeric informations
		const divinfo = document.createElement('div');
		divinfo.id = def+'InfoBox';
		divinfo.className = 'property-info';
		divfilter.appendChild(divinfo);
		const inputedit = document.createElement('input');
		inputedit.id = 'edit-'+def;
		inputedit.className = 'editor-input';
		inputedit.setAttribute('type','number');
		inputedit.setAttribute('step',stepdata);
		divinfo.appendChild(inputedit);
		const inputmin = document.createElement('input');
		inputmin.id = 'min-'+def;
		inputmin.setAttribute('type','number');
		inputmin.setAttribute('min',p[def]['min']);
		inputmin.setAttribute('max',p[def]['max']);
		inputmin.setAttribute('value',p[def]['min']);
		inputmin.setAttribute('step',stepdata);
		inputmin.addEventListener('input',function() {
			const inputval = document.getElementById('min-'+def).value;
			const inputval2 = document.getElementById(def+'SliderInputB').value;
			document.getElementById(def+'SliderInputA').value = inputval;
			p[def]['currMin'] = inputval;
			if (p[def]['min'] == inputval && p[def]['max'] == inputval2) {
				p[def]['changed'] = false;
			} else {
				p[def]['changed'] = true;
			}
			evaluate();
		});
		divinfo.appendChild(inputmin);
		const separator = document.createElement('span');
		separator.className = 'filterseparator';
		separator.textContent = ' ⟷ ';
		divinfo.appendChild(separator);
		const inputmax = document.createElement('input');
		inputmax.id = 'max-'+def;
		inputmax.setAttribute('type','number');
		inputmax.setAttribute('min',p[def]['min']);
		inputmax.setAttribute('max',p[def]['max']);
		inputmax.setAttribute('value',p[def]['max']);
		inputmax.setAttribute('step',stepdata);
		inputmax.addEventListener('input',function() {
			const inputval = document.getElementById('max-'+def).value;
			const inputval2 = document.getElementById(def+'SliderInputA').value;
			document.getElementById(def+'SliderInputB').value = inputval;
			p[def]['currMax'] = inputval;
			if (p[def]['max'] == inputval && p[def]['min'] == inputval2) {
				p[def]['changed'] = false;
			} else {
				p[def]['changed'] = true;
			}
			evaluate();
		});
		divinfo.appendChild(inputmax);
		divinfo.appendChild(document.createTextNode((p[def]['unit'] !== undefined) ? p[def]['unit'] : ''));


		// Sliders
		const sliderbox = document.createElement('div');
		sliderbox.id = def+'Slider';
		sliderbox.className = 'slider-box property-slider';
		divfilter.appendChild(sliderbox);
		const slidera = document.createElement('input');
		slidera.id = def+'SliderInputA';
		slidera.className = 'slider';
		slidera.setAttribute('type','range');
		slidera.setAttribute('min',p[def]['min']);
		slidera.setAttribute('max',p[def]['max']);
		slidera.setAttribute('value',p[def]['min']);
		slidera.setAttribute('step',stepdata);
		slidera.addEventListener('input',function() {
			const inputval = document.getElementById(def+'SliderInputA').value;
			const inputval2 = document.getElementById(def+'SliderInputB').value;

			document.getElementById('min-'+def).value = inputval;
			p[def]['currMin'] = inputval;
			if (p[def]['min'] == inputval && p[def]['max'] == inputval2) {
				p[def]['changed'] = false;
			} else {
				p[def]['changed'] = true;
			}
			evaluate();
		});
		sliderbox.appendChild(slidera);
		const sliderb = document.createElement('input');
		sliderb.id = def+'SliderInputB';
		sliderb.className = 'slider';
		sliderb.setAttribute('type','range');
		sliderb.setAttribute('min',p[def]['min']);
		sliderb.setAttribute('max',p[def]['max']);
		sliderb.setAttribute('value',p[def]['max']);
		sliderb.setAttribute('step',stepdata);
		sliderb.addEventListener('input',function() {
			const inputval = document.getElementById(def+'SliderInputB').value;
			const inputval2 = document.getElementById(def+'SliderInputA').value;

			document.getElementById('max-'+def).value = inputval;
			p[def]['currMax'] = inputval;
			if (p[def]['max'] == inputval && p[def]['min'] == inputval2) {
				p[def]['changed'] = false;
			} else {
				p[def]['changed'] = true;
			}
			evaluate();
		});
		sliderbox.appendChild(sliderb);

		out.appendChild(divfilter);
	}
	return out;
}

// Create scroll navigation
function createScrollNav(direction) {
	const div = document.createElement('div');
	div.id = 'nav' + direction;
	div.className = 'nav';
	const scrollpage = document.createElement('button');
	scrollpage.id = 'scrollpage' + direction;
	const scrollone = document.createElement('button');
	scrollone.id = 'scrollone' + direction;
	const valscrollpage = document.createElement('input');
	valscrollpage.id = 'val-scrollpage' + direction;
	valscrollpage.setAttribute('type','text');
	valscrollpage.setAttribute('readonly','');
	const valscrollone = document.createElement('input');
	valscrollone.id = 'val-scrollone' + direction;
	valscrollone.setAttribute('type','text');
	valscrollone.setAttribute('readonly','');

	div.appendChild(scrollpage);
	div.appendChild(scrollone);
	div.appendChild(valscrollpage);
	div.appendChild(valscrollone);
	return div;
}

function createVmList() {
	document.getElementById('result').textContent = '';
	document.getElementById('select').textContent = '';
	initCount();

	const select = document.createDocumentFragment();
	const result = document.createDocumentFragment();
	result.appendChild(createScrollNav('prev'));
	let i = 0;
	for (const [key, e] of Object.entries(d)) {

		vmstate[key] = { 'selected' : true, filter : []};

		// find initial element to show when application starting !
		if (i == 0) {
			let initialkey = searchParams.get('key')
			if (!initialkey) initialkey = key;
			document.getElementById('sliderpageid').value = initialkey;
		}

		// Result pannel
		const details = document.createElement('details');
		details.id = key;
		details.setAttribute('open','');
		details.className = 'checkicon';
		const summary = document.createElement('summary');
		const label = document.createElement('label');
		label.className = 'labelname';
		label.setAttribute('title',key);
		let title = '';
		if (typeof e.concept !== 'undefined' && c[e.concept].hasOwnProperty('title')) {
			const titles = c[e.concept].title;
			for (let x = 0; x < titles.length; x++) {
				const t = e[titles[x][1]];
				title += (typeof t !== 'undefined' ? t : '') + ' ';
			}
		}
		else
			title = key;
		label.textContent = title;
		summary.appendChild(label);
		details.appendChild(summary);
		details.appendChild(eDetails(key, e));
		result.appendChild(details);

		// Select pannel
		const div = document.createElement('div');
		div.id = 'sel-'+key;
		div.addEventListener('click', function() {
			document.getElementById('sliderpageid').value = key;
			evaluate();
		});
		const labelsel = document.createElement('label');
		labelsel.className = 'labelname';
		div.appendChild(labelsel);
		const span = document.createElement('span');
		span.className = 'numname';
		span.textContent = i+1;
		labelsel.appendChild(span);
		labelsel.appendChild(document.createTextNode(title));
		select.appendChild(div);

//			Event listener on details (opening / closing) TODO remove ?
//			document.getElementById(key).addEventListener('toggle', function() {
//				if ($('#'+key).prop('open')) {
////					vmstate[key].push('selected');
//					vmstate[key]['selected'] = true;

////				console.log(vmstate);
//				}
//				else
//					vmstate[key]['selected'] = false;
//				evaluate();
//			});
		i++;
	};
	result.appendChild(createScrollNav('next'));
	document.getElementById('result').appendChild(result);
	document.getElementById('select').appendChild(select);
	updateCount();
	document.getElementById('sliderpagenum').value = document.getElementById('sliderpagerange').value;
	evaluate(); //  charge initial value at starting
}

// Detail of an entity
function eDetails(vmKey, e) {
	const out = document.createDocumentFragment();
	const dl = document.createElement('dl');

	validcount +=1 ;
	for (const [key, definition] of Object.entries(p)) {
		let value = undefined;
		const dt = document.createElement('dt');
		const dd = document.createElement('dd');
		let colwidth;

		// DYNAMIC FIELDS
		if (definition.hasOwnProperty('fn')) {
//			const calcf = new Function('e','return '+p[key]['fn']);

			let paramsok = true;
			for (const val of p[key]['fn_params']) {
				if (!e.hasOwnProperty(val) || e[val] === 'undefined' ) {
					paramsok = false;
					break;
				}
			}
			if (paramsok) {
				const calcf = p[key]['fn'];
				const calc = calcf(e);
				switch (definition.type) {
					case 'ref':
						if (Array.isArray(calc)) {
							colwidth = (calc.length > 0) ? Math.trunc(100/calc.length) : 100;
							value = calc.reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
							e[key] = calc;
						}								
						if (typeof calc === 'string' && calc.startsWith("<"))
							value = calc ;
						else
							value = '<label class="property-box txt-'+calc+'">'+calc+'</label>' ;
						break;
					case 'num':
						if (isNumber(calc)) {
							value = calc;
							if (!e.hasOwnProperty(key))
								e[key] = calc;
						}
						else if (Array.isArray(calc)) {
							colwidth = (calc.length > 0) ? Math.trunc(100/calc.length) : 100;
							value = calc.reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
							if (!e.hasOwnProperty(key))
								e[key] = calc;
						}
						break;
				}
			}
		}
		if (typeof value !== 'undefined') {
			dd.innerHTML = value;
		}
//		else {
			if (e.hasOwnProperty(key)) {
				let tooltip = '';
				if (definition.type == 'imageurl') {
					const div = document.createElement('div');
					div.className = 'prop-'+key;
					div.setAttribute('style','background-image:url(\''+e[key]+'\');');
					const div2 = document.createElement('div');
					div.className = 'gradient-image';
					out.appendChild(div.appendChild(div2));
				}
				switch (definition.type) {
					case 'checkbox':
						/* checkbox data does not need to be an object if it has just one value */
						if (typeof e[key] !== 'object') {
							let makeObject = e[key];
							e[key] = [ makeObject ];
						}
						
						e[key].forEach(function(val, index) {
							if (typeof value === 'undefined')
								value = '<label for="'+key+'-'+val+'" class="property-box txt-'+key+'-'+val+'"></label>';
							else
								value += '<label for="'+key+'-'+val+'" class="property-box txt-'+key+'-'+val+'"></label>';

						});
	//						tooltip = 'title="'+value+'"';
						break;
					case 'radio':
						value = e[key];
						break;
					case 'range':
						value = e[key];
						if (definition.hasOwnProperty('unit')) {
							value += ' ' + definition.unit;
						}
						break;
					case 'info':
					case 'text':
						value = e[key];
						break;
					case 'link':
						value = '<a href="'+e[key]['url']+'" target="_blank">'+e[key]['label']+'</a>';
						break;
					case 'imageurl':
						value = '<img src="'+e[key]+'">';
						break;

	// NEW SECTION DATABASE !! BEGIN
					case 'ref':
						if (!key.includes('_')) {
//						console.log(key,e);
							colwidth = (e[key].length > 0) ? Math.trunc(100/e[key].length) : 100;
							value = e[key].reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'"><label class="property-box txt-'+((b.indexOf('?') != -1) ? b.substr(0,b.indexOf('?')) : b)+'">'+b.substr(0,b.indexOf('?'))+b.substr(b.indexOf('=')+1)+'</label></span>',''); // TODO this code works only with 1 query param
						}
						else {
							const prefix = key.substr(0, key.indexOf('_'));
							colwidth = (e[prefix].length > 0) ? Math.trunc(100/e[prefix].length) : 100;
							value = e[key].reduce((a,b,i) => a + '<span style="width:'+colwidth+'%" class="part">'+b.reduce((c,d) => c + '<label class="property-box txt-'+((d.indexOf('?') != -1) ? d.substr(0,d.indexOf('?')) : d)+'">'+d.substr(0,d.indexOf('?'))+d.substr(d.indexOf('=')+1)+'</label>','')+'</span>',''); // TODO this code works only with 1 query param
						}
						break;
					case 'num':
						if (!key.includes('_')) {
							if (typeof value === 'undefined') {
								if (Array.isArray(e[key])) {
									colwidth = (e[key].length > 0) ? Math.trunc(100/e[key].length) : 100;
									value = e[key].reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
								}
								else
									value = e[key];
							}
							else {
								if (Array.isArray(e[key])) {
									colwidth = (e[key].length > 0) ? Math.trunc(100/e[key].length) : 100;
									value = e[key].reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
								}
								else {
									if (value == 0) {
//								console.log('coucou',key,value,	e[key]);
										value = e[key];
									}
									if (definition.hasOwnProperty('unit')) {
										value += ' ' + definition.unit;
									}
//								console.log('coucou2',key,value,	e[key]);
								}
							}
						}
						else {
							if (Array.isArray(e[key])) {
								const prefix = key.substr(0, key.indexOf('_'));
								colwidth = (e[prefix].length > 0) ? Math.trunc(100/e[prefix].length) : 100;
								value = e[key].reduce((a,b,i) => a + '<span style="width:'+colwidth+'%"class="part ref-'+e[prefix][i]+'">'+b.reduce((c,d) => c + d + ', ','').slice(0, -2)+'</span>','');
							}
						}
						break;
					case 'txt':
					case 'text':
						if (Array.isArray(e[key])) {
							colwidth = (e[key].length > 0) ? Math.trunc(100/e[key].length) : 100;
							value = e[key].reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
						}
						else {
							value = e[key];
						}
						break;
					case 'bool':
						if (Array.isArray(e[key])) {
							colwidth = (e[key].length > 0) ? Math.trunc(100/e[key].length) : 100;
							value = e[key].reduce((a,b) => a + '<span style="width:'+colwidth+'%" class="part ref-'+key+'">'+b+'</span>','');
						}
						else {
							value = e[key];
						}
						break;

	// NEW SECTION DATABASE !! END
				}

				dt.className = 'prop-'+key+' txt-'+key;	
				dd.className = 'prop-'+key;	
				
				if (definition.type == 'checkbox')
					dd.innerHTML = value;
				else if (definition.type == 'radio') {
					const label = document.createElement('label');
					label.className = 'property-box txt-'+key+'-'+value;
					label.setAttribute('for',key+'-'+value)
					dd.appendChild(label);
				}
				else
					dd.innerHTML = value;
			}
			else {
				dt.className = 'prop-nodata prop-'+key;	
				dd.className = 'prop-nodata prop-'+key;	
			}
//		}
		dl.appendChild(dt);
		dl.appendChild(dd);
	}
	dl.className = 'itemboxes';
	out.appendChild(dl);
	return out;	
}

function evaluate() {
	initCount();
	let vmnum = 0;
	vmcursor = [];
	for (const [key, value] of Object.entries(d)) {
		const visibility = vmVisibility(key,value);
		document.getElementById(key).className = visibility;
		document.getElementById('sel-'+key).className = visibility;
		vmnum += 1;
		vmVisibilityPage(key, vmnum);
	}
	for (const [key, value] of Object.entries(p)) {
		document.getElementById(key+'Area').classList.remove('property-changed');
		if (value.changed)
			document.getElementById(key+'Area').classList.add('property-changed');

// TODO indicateur si modif dans les filtres
//			if (value.changed && !$('#reload').hasClass('property-changed')) {
//				$('#reload').addClass('property-changed');
//			}
//			else if (value.changed && $('#reload').hasClass('property-changed')) {
//				$('#reload').removeClass('property-changed');
//			}
	}
	updateCount();
}

function vmVisibility(vmkey,e) {
	let visibility = true;
	let uncertain = false;
	for (const [key, def] of Object.entries(p)) {
		if (def.changed && !e.hasOwnProperty(key)) {
			uncertain = true;
			uncertaincount += 1;
			return 'uncertain';
		}

		if (e.hasOwnProperty(key)) {
			switch(def.type) {
				case 'range':
					if (def.currMin > e[key] || def.currMax < e[key]) {
						visibility = false;
					}
					break;

				case 'radio':
				case 'checkbox':
					let checkVisibility = false;
					let checkedNothing = true;
					def.values.forEach(function(vkey,index) {
						const checkId = key+'-'+vkey;
						if (document.getElementById(checkId).checked) {
							checkedNothing = false;
							if (
//								$.inArray(vkey, e[key]) > -1 // checkboxes
								e[key].indexOf(vkey) > -1 // checkboxes
								|| vkey == e[key] //radio
							) {
								checkVisibility = true;
							}
						}
					})
					visibility = visibility && (checkedNothing || checkVisibility);
					break;
				case 'text':
//				if (Array.isArray(e[key])) console.log(def.textfilter,e[key].flat(Infinity).join(''));
					if ((def.textfilter !== undefined && def.textfilter != '') &&
						((Array.isArray(e[key]) && !e[key].flat(Infinity).join('').toLowerCase().includes(def.textfilter)) ||
						(!Array.isArray(e[key]) && !e[key].toLowerCase().includes(def.textfilter))) ) {
						visibility = false;
					}
					break;
			}
		}
	}
	if (!uncertain) {
		if (visibility) {
			vmstate[vmkey]['filter'] = 'valid';
			validcount +=1 ;
		}
		else {
			vmstate[vmkey]['filter'] = 'invalid';
			invalidcount += 1;
		}
	}
	else {
		vmstate[vmkey]['filter'] = 'uncertain';
	}

	return uncertain ? 'uncertain' : ( visibility ? 'valid' : 'invalid' );
}

function vmVisibilityPage(key, vmnum) {
	const maxcolumns = parseInt(window.getComputedStyle(document.getElementById('content')).getPropertyValue('--maxcolumns')) || 2;
	const cursor = parseInt(document.getElementById('sliderpagerange').value);
	const cursorkey = document.getElementById('sliderpageid').value;
	
	if (key == cursorkey) {
		document.getElementById('sliderpagerange').value = vmnum + '';
		document.getElementById('sliderpagenum').value = vmnum + '';
		document.getElementById('sel-'+key).classList.toggle('cursor');
		if (typeof vmcursor[0] === 'undefined')
			document.getElementById('val-scrolloneprev').value = key;
		else
			document.getElementById('val-scrolloneprev').value = vmcursor[0];

		for (let i = 0; i < maxcolumns ; i++) {
			if (typeof vmcursor[maxcolumns-1-i] === 'undefined')
				document.getElementById('val-scrollpageprev').value = key;
			else {
				document.getElementById('val-scrollpageprev').val = vmcursor[maxcolumns-1-i];
				break;
			  }
		}
	}
	if (pagecount == 1) {
		document.getElementById('val-scrollonenext').value = key;
	}
	if (pagecount == maxcolumns) {
		document.getElementById('val-scrollpagenext').value = key;
		pagecount++;
	}
	if (pagecount > 1 && pagecount < maxcolumns) {
		document.getElementById('val-scrollpagenext').value = key;
	}

	// Visibility of entry
	const optionstate = fn_bool[fn_namefull.indexOf('option'+vmstate[key].filter)];
	if (	key == cursorkey ||
		(
		pagecount > 0 &&
		pagecount < maxcolumns &&
		optionstate
		)
		) {
		pagecount++;
		document.getElementById(key).classList.toggle('visiblepage');
		document.getElementById('sel-'+key).classList.toggle('visiblepage');
	}
	else
		document.getElementById(key).classList.remove('visiblepage');

	if (optionstate)
		vmcursor.unshift(key);
}

function build() {
	for (const def in p) {
		for (const e in d) {
			if (p[def].hasOwnProperty('enrich')) {
				p[def].enrich(d[e]);
			}

			switch (p[def]['type']) {
				case 'checkbox':
				case 'radio':
					break;
				case 'range':
					p[def]['min'] = Math.min((p[def]['min'] !== null && p[def]['min'] !== undefined) ? p[def]['min'] : 9999, ((d[e][def] !== null && d[e][def] !== undefined) ? d[e][def] : 9999));
					p[def]['max'] = Math.max((p[def]['max'] !== null && p[def]['max'] !== undefined) ? p[def]['max'] : 0, ((d[e][def] !== null && d[e][def] !== undefined) ? d[e][def] : 0));
					p[def]['currMin'] = p[def]['min'];
					p[def]['currMax'] = p[def]['max'];
					break;
			}
		}
		p[def]['changed'] = false;
	}
	document.getElementById('choice').innerHTML += '<div id="sliderpage" class="slider-box"><input type="number" min="1" max="'+d.length+'" id="sliderpagenum" readonly><input type="text" id="sliderpageid"><input type="range" min="1" max="'+d.length+'" value="1" class="slider" id="sliderpagerange"></div>';
	document.getElementById('sliderpagerange').addEventListener('input',function() {
		document.getElementById('sliderpagenum').value = document.getElementById('sliderpagerange').value;
		evaluate();
	});
	document.getElementById('sliderpagenum').addEventListener('input',function() {
		document.getElementById('sliderpagerange').value = document.getElementById('sliderpagenum').value;
		evaluate();
	});
	document.getElementById('sliderpageid').addEventListener('input',function() {
		evaluate();
	});


	// Add boilerplate class to link --txt-propsFields with .txt-propsFields
	const style = document.createElement('style');
	style.id = "classtxtstyle" ; // so you can get and alter/replace/remove later
	const choice = document.createDocumentFragment();

	for (const cat in t) {
		const trait = document.createElement('div');
		trait.id = 'impl-'+cat;
		trait.className = 'impl';
		const summary = document.createElement('summary');
		summary.className = 'impl-summary';
		trait.appendChild(summary);
		const label = document.createElement('label');
		label.textContent = cat;
		summary.appendChild(label);
		const traitbox = document.createElement('div');
		traitbox.id = 'implbox-'+cat;
		traitbox.className = 'implbox';
		trait.appendChild(traitbox);
		choice.appendChild(trait);

		for (const def in t[cat]) {
			const divarea = document.createElement('div');
			divarea.id = def+'Area';
			divarea.className = 'vm-property';
			const divtitle = document.createElement('div');
			divtitle.id = def+'Title';
			divtitle.className = 'text txt-'+def;
			divarea.appendChild(divtitle);

			// add default text for var
			document.documentElement.style.setProperty('--txt-'+def, '"'+def+'"');
			// add default class correspondant with var
			style.textContent += '.txt-' + def + ':after {content:var(--txt-' + def + ');} ';

			switch (p[def]['type']) {
				case 'checkbox':
				case 'radio':
					divarea.appendChild(createCheckbox(def,style));
					break;
				case 'range':
					divarea.appendChild(createSlider(def));
					break;
				case 'text':
				case 'link':
				case 'imageurl':
					divarea.appendChild(createText(def));
					break;
			}
			traitbox.appendChild(divarea);
		}
		document.getElementById('choice').appendChild(choice);
	}
	document.getElementsByTagName("HEAD")[0].appendChild(style) ;

	createVmList();
}

//		document.documentElement.style.setProperty('--totalcolumns', '20');

//// Interface state vars
//let vmstate = {};
//let vmcursor = [];
//let lettershortcuts = true;
//let pagecount = 0;
//let selectcount = 0;
//let validcount = 0;
//let invalidcount = 0;
//let uncertaincount = 0;

//const searchParams = new URLSearchParams(window.location.search)
//let lang = searchParams.get('lang')
//const userLang = navigator.language || navigator.userLanguage;
//document.addEventListener('DOMContentLoaded', (event) => {
//    console.log('DOM fully loaded and parsed');
//	if (!lang) lang = userLang.substring(0,2);

//	loadLang(lang);
//	initSettings('webapp');
//	addKeyboardListener();
//	createProjectInformationsJS();
//// loadVars not necessary for JS data file. Needed if JSON file
//	loadVars('trait',p,t);
//	loadVars('concept',c);
//	build();
//	addClickListener();
//	addChangeListener();
//});
