'use strict';{const data_append = [
// FUNCTIONS


{	F				: true,
	n				: "requestfullscreen",
	desc			: "display in fullscreen|en",
	lang			: "es",
//	input			: [""],
	code			: `
	const doc = window.document;
	const docEl = doc.documentElement;

	const requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
	const cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

	if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
		requestFullScreen.call(docEl);
		g[0]['fullscreenselect'] = true;
	}
	else {
		cancelFullScreen.call(doc);
		g[0]['fullscreenselect'] = false;
	}
	`,
},
{	F				: true,
	n				: "save",
	desc			: "save database to localstorage|en",
	lang			: "es",
//	input			: [""],
//	code			: "",
},
{	F				: true,
	n				: "load",
	desc			: "load database from localstorage|en",
	lang			: "es",
//	input			: [""],
//	code			: "",
},
{	F				: true,
	n				: "dom",
	desc			: "return dom element from id|en",
	lang			: "es",
	input			: ["id"],
	code			: "return document.getElementById(id);",
},
{	F				: true,
	n				: "domwindow",
	desc			: "return window dom|en",
	lang			: "es",
	code			: "return window;",
},
{	F				: true,
	n				: "setcssvar",
	desc			: "set css var",
	lang			: "es",
	input			: ["varname","varvalue"],
	code			: `
	if (typeof varvalue === 'number') document.documentElement.style.setProperty('--'+varname,varvalue);
	else document.documentElement.style.setProperty('--'+varname, '"'+varvalue+'"');
	g[0]['--'+varname] = varvalue;
	`,
},
{	F				: true,
	n				: "addcssvar",
	desc			: "set css var (cumulative if value numeric value)",
	lang			: "es",
	input			: ["varname","varvalue"],
	code			: `
//	const prevvalstr = getComputedStyle(document.documentElement).getPropertyValue('--'+varname);
//		const prevval = parseFloat(prevvalstr);
//		console.log("prevval",prevval);
//		if (typeof varvalue === 'number') document.documentElement.style.setProperty('--'+varname,prevval + varvalue);

	let prevval = null;
	const prevvalg = g[0]['--'+varname];
	if (prevvalg)
		prevval = prevvalg;
	else {
		const prevvalstr = getComputedStyle(document.documentElement).getPropertyValue('--'+varname);
		if (prevvalstr != '' && isNumber(prevvalstr))
			prevval = parseFloat(prevvalstr);
		else
			prevval = prevvalstr;
	}
	// TODO units
	g[0]['--'+varname] = prevval + varvalue;
	if (typeof prevval === 'number' && typeof varvalue === 'number')
		document.documentElement.style.setProperty('--'+varname,prevval + varvalue);
	else
		document.documentElement.style.setProperty('--'+varname,'"' + prevval + varvalue + '"');
	`,
},
{	F				: true,
	n				: "switchview",
	desc			: "switch view mode",
	lang			: "es",
	input			: ["view"],
	code			: `document.getElementById('global').className = view;g[0]['viewmode'] = view;`,
},

{	F				: true,
	n				: "createMenu",
	desc			: "create menu buttons",
	lang			: "es",
	input			: ["addlistener"],
	code			: `
	const query = q.menubar[1],
		identify = getarr(query,'identify.n'),
		func = getarr(query,'callesfn.n',2),
		funcname = getarr(query,'callesfn.n',0),
		click = getarr(query,'listenmouse.click'),
		keydown = getarr(query,'listenkeyboard.keydown'),
		keyup = getarr(query,'listenkeyboard.keyup'),
		from = getarr(query,'listenmouse.from'),
		textlabel = getarr(query,'listenmouse.textlabel'),
		backimage = getarr(query,'listenmouse.backgroundimage');

	const menu = document.createDocumentFragment();
	const chkstart = document.createElement('input');
	chkstart.id = 'chk-viewallmenu';
	chkstart.setAttribute('type','checkbox');
	document.getElementById('global').insertBefore(chkstart,document.getElementById('menu'));

//	const btnstart = document.createElement('label');
//	btnstart.id = 'viewallmenu';
//	btnstart.setAttribute('for','chk-viewallmenu');
//	menu.appendChild(btnstart);

//	const btnsettings = document.createElement('label');
//	btnsettings.id = 'btn-switchviewsettings';
////	btnsettings.setAttribute('for','chk-viewallmenu');
//	btnsettings.appendChild(document.createTextNode('⚙'));
//	menu.appendChild(btnsettings);

	for (let i = 0; i < query.LENGTH ; i++) {
			console.log("coucou createenu");

		const btn = document.createElement('button'),
			fnname = getfnname(funcname[i]);

		btn.id = from[i];

		btn.title = identify[i] + ' : ';
		if (fnname && f[fnname] && f[fnname].desc) btn.title += stringtolang(f[fnname].desc)+'\\n'
		if (keydown[i]) btn.title += 'shortcut : ' + keydown[i].join(' or ');

		if (addlistener && func[i]) { // BUG if (addlistener) does not work, we must use addlistener == true
			btn.addEventListener('click',function(ev) {
				func[i][0](...(typeof func[i][1] !== 'undefined' ? [...func[i][1],ev] : [ev]));
			});
//				func[i][0]);
//				console.log('bind',func[i][0]);
		}


		if (backimage[i]) {
			const imageurl = backimage[i];
			if (imageurl.startsWith('url('))
				btn.style.backgroundImage = backimage[i];
			else {
				const icon = document.createElement('span');
				icon.textContent = imageurl;
				icon.className = 'btnicone'
				btn.appendChild(icon);
			}
		}
		const text = document.createElement('span');
		text.className = 'btntext'
		text.textContent += textlabel[i] ? textlabel[i] : identify[i];
		btn.appendChild(text);

		menu.appendChild(btn);

	}
	if (document.getElementById('menu') !== null) document.getElementById('menu').appendChild(menu);
`,
},
{	F				: true,
	n				: "createSettings",
	desc			: "create settings space",
	lang			: "es",
	input			: ["addlistener"],
	code			: `
	const query = q.init,
		identify = getarr(query,'identify.n'),
		func = getarr(query,'callesfn.n',2),
		click = getarr(query,'listenmouse.click'),
		keydown = getarr(query,'listenkeyboard.keydown'),
		keyup = getarr(query,'listenkeyboard.keyup'),
		from = getarr(query,'listenmouse.from'),
		textlabel = getarr(query,'listenmouse.textlabel');

	const settings = document.createDocumentFragment();
	const dl = document.createElement('dl');
	for (let i = 0; i < query.LENGTH ; i++) {
		if (func[i]) {
			const dt = document.createElement('dt');
			const dd = document.createElement('dd');
			
			const btn = document.createElement('button');
			if (from[i]) btn.id = from[i];
			if (addlistener && func[i]) { // BUG if (addlistener) does not work, we must use addlistener == true
				btn.addEventListener('click',function(ev) {
					func[i][0](...(typeof func[i][1] !== 'undefined' ? [...func[i][1],ev] : [ev]));
				});
//				func[i][0]);
//				console.log('bind',func[i][0]);
			}
			dt.appendChild(btn);

			const span = document.createElement('span');
			span.textContent = textlabel[i] ? textlabel[i] : identify[i];
			btn.appendChild(document.createTextNode('F'));
			dt.appendChild(span);

			const spanright = document.createElement('span');
			spanright.className = 'floatright';
			let html = '';
			if (keydown[i]) {
				for (const key of keydown[i]) {
					if (key.startsWith('[')) {
						const keyarr = key.substr(1).split(']');
						html += '<span class="key">'+keyarr[0]+'</span>+';
						html += '<span class="key">'+keynametotext(keyarr[1])+'</span>';
					}
					else html += '<span class="key">'+keynametotext(key)+'</span>';
					html += ' / ';
				}
			}
			spanright.innerHTML = html.slice(0,-3);
			dt.appendChild(spanright);
			dl.appendChild(dt);
			dl.appendChild(dd);
		}
	}
	settings.appendChild(dl);
	if (document.getElementById('settings') !== null) document.getElementById('settings').appendChild(settings);
`,
},
{	F				: true,
	n				: "createProjectInformation",
	desc			: "create text description of project in webapp|en",
	lang			: "es",
	code			: `
	const query = q.gravitonweb[1];
		readme = getarr(query,'project.readme'),
		lastcommit = getarr(query,'project.lastcommit');

	for (let i = 0; i < query.LENGTH ; i++) {
		if (readme && readme[i]) {
			if (document.getElementById('readme') !== null)
				document.getElementById('readme').innerHTML = convertMarkdownHtml(readme[i]).trim();
			foundreadme = true;
		}
		if (lastcommit && lastcommit[i]) {
			const datelastact = new Date(lastcommit[i]['date']);
			if (document.getElementById('project-lastactivity') !== null)
				document.getElementById('project-lastactivity').innerHTML = '<ul><li>' + datelastact.toDateString() + ' ' + datelastact.toLocaleTimeString()+'</li><li>'+lastcommit[i]['description']+'</li></ul>';
			foundcommit = true;
		}
	}
	`,
},
{	F				: true,
	n				: "createProjectsLauncher",
	desc			: "create entry for launching projects|en",
	lang			: "es",
	code			: `
	const query = q.projects[1];
		identify = getarr(query,'identify.n'),
		querysdata = getarr(query,'project.query'),
		imgname = getarr(query,'project.backgroundimage'),
		readme = getarr(query,'project.readme'),
		lastcommit = getarr(query,'project.lastcommit'),
		dom = document.createDocumentFragment();
	for (const name of Object.keys(q)) {
		const label = document.createElement('label');
		label.className = 'app';
		label.addEventListener('click',function(ev) {
			createQuery('app.'+name);
		});
		dom.appendChild(label);
		
		const div = document.createElement('div');
		div.textContent = 'app';
		div.className = 'projectname';
		label.appendChild(div);

		const div2 = document.createElement('div');
		div2.textContent = name;
		div2.className = 'projectdata';
		label.appendChild(div2);
	}
	for (let i = 0; i < query.LENGTH ; i++) {
		if (identify[i]) {
			for (const querydata of querysdata[i]) {
				const queryidarr = querydata.split('.');
				if (queryidarr[0] != 'app') {
					const label = document.createElement('label');
					label.className = (queryidarr[1] == 'init') ? 'app' : 'project';
					if (readme[i]) label.setAttribute('title',readme[i]);
					label.addEventListener('click',function(ev) {
//					console.log(querydata);
						createQuery(querydata);
					});
					dom.appendChild(label);

					if (imgname[i]) {
						const div = document.createElement('div');
						div.className = 'projectimage';
						if (imgname[i].startsWith('url('))
							div.style.backgroundImage = imgname[i];
						else
							div.textContent = imgname[i];
						label.appendChild(div);
					}

					const div = document.createElement('div');
					div.textContent = identify[i];
					div.className = 'projectname';
					label.appendChild(div);

					const div2 = document.createElement('div');
					div2.textContent = querydata;
					div2.className = 'projectdata';
					label.appendChild(div2);
					
				}
			}
		}
	}
	const container = document.getElementById('txt-projects');
	container.appendChild(dom);
	`,
},
{	F				: true,
	n				: "jump",
	desc			: "move one/page next/prev entry|en",
	lang			: "es",
	code			: `console.log('jump',g.id);`,
},

{	F				: true,
	n				: "editor",
	desc			: "prepare editor mode|en",
	lang			: "es",
	code			: "console.log('coucou');",
},
{	F				: true,
	n				: "cleanlocaldata",
	desc			: "clean local storage data|en",
	lang			: "es",
},

// QUERIES

//{	Q				: "project",
//	filter			: [ { equals : { "project": true, "identify.n": "graviton-web" } } ],
//},
{	Q				: "menubar",
	filter			: [ { equals : { "favorite": true, "callesfn":true } } ],
	sorted			: [ {"favorite.order": "up" } ],
	order			: [ "identify", "favorite", "*", "callesfn", "startfn" ],
},
{	Q				: "projects",
	filter			: [ { equals : { "project": true } } ],
},
{	Q				: "gravitonweb",
	filter			: [ { equals : { "project": true, "identify.n": "graviton-web" } } ],
//	filter			: [ { equals : { "listenmouse": true } } ],
},
{	identify		: { n : "graviton-web" },
	project			: { query: ["app.init"] },
},
{	identify		: { n : "universe" },
	project			: { readme: "Universe database", query: ["data.init", "data.object"],backgroundimage:"42"},
},
{	identify		: { n : "transport" },
	project			: { readme: "Transport project database", query: ["data.humanpoweredvehicle"],backgroundimage:"url('data/humanpoweredvehicle.webp')", },
},

// TRAITS

{	T				: "project",
	readme			: {},
	lastcommit		: {},
	query			: {},
	backgroundimage	: {},
},
{	T				: "favorite",
	order			: { defaultvalue: 10},
},

// GLOBAL VARS
{	G				: true,
	id				: "",
	nextid			: "",
	previd			: "",
	nextpageid		: "",
	prevpageid		: "",
	keyscactive		: true,
	columnview		: 6,
	pagecount		: 6,
	lastkey			: "",
},


// STARTING FN

{	identify			: { n	 	: "createProjectInformation" },
	callesfn			: {	n		: "=createProjectInformation" },
	startfn				: true,
},
{	identify			: { n	 	: "createProjectsLauncher" },
	callesfn			: {	n		: "=createProjectsLauncher" },
	startfn				: true,
},
{	identify			: { n	 	: "createMenu" },
	callesfn			: {	n		: "=createMenu~true" },
	startfn				: true,
},
{	identify			: { n	 	: "createSettings" },
	callesfn			: {	n		: "=createSettings~true" },
	startfn				: true,
},
{	identify			: { n	 	: "datalist" },
	callesfn			: {	n		: "=switchview~mode-explorer" },
	listenkeyboard		: {	keydown	: ["F3"] },
	listenmouse			: {	click	: true , backgroundimage: "◫"},
//	favorite			: { order	: 3 },
//	listenmouse			: {	click	: true, textlabel : "▽▼▿▾▷▶▹▸△▲▵▴◁◀◃◂ ⌂" },
	favorite			: { order	: 9 },
},
{	identify			: { n	 	: "switchviewinit" },
	callesfn			: {	n		: "=switchview~mode-init" },
	listenkeyboard		: {	keydown	: ["I"] },
},
{	identify			: { n	 	: "home" },
	callesfn			: {	n		: "=switchview~mode-start" },
	listenkeyboard		: {	keydown	: ["F1"] },
	listenmouse			: {	click	: true, backgroundimage: "𝔾"},
	startfn				: { order	: 100 },
	favorite			: { order	: 1 },
},
{	identify			: { n	 	: "settings" },
	callesfn			: {	n		: "=switchview~mode-settings" },
	listenkeyboard		: {	keydown	: ["F2"] },
	listenmouse			: {	click	: true, backgroundimage: "⚙" },
	favorite			: { order	: 8 },
},

// OTHER
{	identify			: { n		: "zoomout" },
	callesfn			: {	n		: "=addcssvar~nbentityviewadd~1" },
	listenkeyboard		: {	keydown	: ["-"] },
	listenmouse			: {	click	: true, backgroundimage:'−' },
	favorite			: { order	: 6 },
},
{	identify			: { n		: "zoomin" },
	callesfn			: {	n		: "=addcssvar~nbentityviewadd~-1" },
	listenkeyboard		: {	keydown	: ["+"] },
	listenmouse			: {	click	: true, backgroundimage:'+' },
	favorite			: { order	: 7 },
},
{	identify			: { n		: "save" },
	callesfn			: {	n		: "=save" },
	listenkeyboard		: {	keydown	: ["[ctrl]s"] },
	listenmouse			: {	click	: true, backgroundimage:'🖫' },
	favorite			: { order	: 4 },
},
{	identify			: { n		: "fullscreen" },
	callesfn			: {	n		: "=requestfullscreen" },
	listenkeyboard		: {	keydown	: ["F11"] },
	listenmouse			: {	click	: true, backgroundimage:'F' },
//	favorite			: { order	: 5 },
},
{	identify			: { n		: "load" },
	callesfn			: {	n		: "=load" },
	listenkeyboard		: {	keydown	: ["[ctrl]o"] },
	listenmouse			: {	click	: true, backgroundimage:'⛁' },
	favorite			: { order	: 5 },
},
{	identify			: { n		: "edit" },
	callesfn			: {	n		: "=editor" },
	listenkeyboard		: {	keyup	: ["m"] },
//	listenmouse			: {	click	: true, textlabel: "✎" },
	listenmouse			: {	click	: true, backgroundimage:'url("data:image/svg+xml,%3Csvg xmlns=%22http://www.w3.org/2000/svg%22 width=%2220%22 height=%2220%22 viewBox=%220 0 20 20%22%3E%3Ctitle%3Eedit%3C/title%3E%3Cpath fill=%22darkgrey%22 d=%22M16.77 8l1.94-2a1 1 0 000-1.41l-3.34-3.3a1 1 0 00-1.41 0L12 3.23zM1 14.25V19h4.75l9.96-9.96-4.75-4.75z%22/%3E%3C/svg%3E")' },
	listenchange		: true,
	favorite			: { order	: 3 },
},
{	identify			: { n		: "invalid" },
	listenchange		: { bool	: false }  ,
},
{	identify			: { n		: "cleanlocaldataworld" },
	callesfn			: {	n		: "=cleanlocaldata" },
	listenkeyboard		: {	keydown	: ["[ctrl]!"] },
},
//{	identify			: { n	 	: "jumponenext" },
//	callesfn			: {	n		: "=jump" },
//	listenkeyboard		: {	keydown	: ["ArrowRight","d"] },
//	listenmouse			: {	click	:  true },
//},
//{	identify			: { n	 	: "jumponeprev" },
//	callesfn			: {	n		: "=jump" },
//	listenkeyboard		: {	keydown	: ["ArrowLeft","q"] },
//	listenmouse			: {	click	: true },
//},
//{	identify			: { n	 	: "jumppagenext" },
//	callesfn			: {	n		: "=jump" },
//	listenkeyboard		: {	keydown	: ["[ctrl]ArrowLeft"] },
//	listenmouse			: {	click	: true },
//},
//{	identify			: { n	 	: "jumppageprev" },
//	callesfn			: {	n		: "=jump" },
//	listenkeyboard		: {	keydown	: ["[ctrl]ArrowRight"] },
//	listenmouse			: {	click	: true },
//},


];FILES.app = [...FILES.app,...data_append];}

function save() {
	console.log("save");
}
function load() {
	console.log("load");
}
function createQuery(queryid) {
	const queryarr = queryid.split('.');
	console.log(D[3]);
	if (queryarr.length == 2) {
		const query = (queryid == 'app.init') ? q.init : (queryarr[0] == 'app') ? q[queryarr[1]][1] : (queryarr[1] == 'init') ? D[3].init : D[3][queryarr[1]][1],
			identify = getarr(query,'identify.n');
		f.switchview.obj('mode-explorer');
//		console.log(query);
		const props = document.createElement('dl'),
			vals = document.createDocumentFragment();
		let dl2 = null;

		const edom = document.createElement('dl');

		let dtraitbox = document.createElement('div');
		dtraitbox.className = 'traitbox trait-INDEX';
		
		const dtrait = document.createElement('div'),
			dprop = document.createElement('div'),
			dinfo = document.createElement('div'),
			dinfo2 = document.createElement('div');

		for (let i = 0; i < query.LENGTH ; i++) {
			const dval = document.createElement('div');
			dval.className = 'dn test-trait-INDEX hastrait';
			dtraitbox.appendChild(dval);
			
			const dval2 = document.createElement('div');
			dval2.className = 'dn hastrait';
			dval2.textContent = (i+1) + '';
			dtrait.appendChild(dval2);
		}
		dinfo.className = 'dn traitinfo';
		dinfo.textContent = queryid;
		dtrait.appendChild(dinfo);
		
		dinfo2.className = 'dn traitinfo';
		dinfo2.textContent = query.LENGTH;
		dinfo.appendChild(dinfo2);
		
		dtrait.className = 'trait';
		dtraitbox.appendChild(dtrait);
		vals.appendChild(dtraitbox);
		
		for (const [prop,val] of Object.entries(query)) {
			if (prop != 'LENGTH') {

				const proparr = prop.split('.'),
					dtrait = document.createElement('div'),
					dprop = document.createElement('div'),
					dinfo = document.createElement('div'),
					dinfo2 = document.createElement('div');

				if (proparr.length == 1) {
					dtraitbox = document.createElement('div');
					dtraitbox.className = 'traitbox trait-' + prop;

					let prevtextcontent = '';
					for (let i = 0; i < query.LENGTH ; i++) {
						const dval = document.createElement('div');
						let strclass = 'dn test-trait-' + prop
						if (val[0][i]) strclass += ' hastrait';
						else strclass += '';
						dval.className = strclass;
						dtraitbox.appendChild(dval);
						
						const dval2 = document.createElement('div');
						strclass = 'dn';
						if (val[0][i]) {
							strclass += ' hastrait';
							if (prevtextcontent != prop) dval2.textContent = prop;
							prevtextcontent = prop;
						}
						else {
							strclass += ' no';
							prevtextcontent = '';
						}
						dval2.className = strclass;
						dtrait.appendChild(dval2);

					}
					dinfo.className = 'dn traitinfo';
					dinfo.textContent = prop;
					dtrait.appendChild(dinfo);

					dinfo2.className = 'dn';
					if (val[1] && Array.isArray(val[1]['true'])) dinfo2.textContent = val[1]['true'].length;
					dinfo.appendChild(dinfo2);

					dtrait.className = 'trait';
					dtraitbox.appendChild(dtrait);
					vals.appendChild(dtraitbox);
				}
				else {
					for (let i = 0; i < query.LENGTH ; i++) {
						const dval = document.createElement('div');
//							br = document.createElement('br');
//						br.className = 'mobile';
//						dval.appendChild(br);
						let strclass = 'dn';
						// no data given, using trait default value (execute true)
						if (val[1][i] !== null && val[0][i] === null) {
//							const span = document.createElement('span');
//							span.textContent = displayval(val[1][i]);
//							dval.appendChild(span);
							dval.setAttribute('title',displayval(val[1][i]));
							createfromval(dval, val[1][i]);
							strclass += ' hasvalue defaultvalue';
						}
						// has data
						else if (val[1][i] !== null) {
//							const span = document.createElement('span');
//							span.textContent = displayval(val[1][i]);
//							dval.appendChild(span);
							dval.setAttribute('title',displayval(val[1][i]));
							createfromval(dval, val[1][i]);
							strclass += ' hasvalue';
						}
						// no data given, using trait default value (execute false)
						else if (val[2][i] !== null) {
//							const span = document.createElement('span');
//							span.textContent = displayval(val[0][i]);
//							dval.appendChild(span);
							dval.setAttribute('title',displayval(val[0][i]));
							createfromval(dval, val[0][i]);
							strclass += ' hasvalue';
						}
						// no data but has trait
						else if (query[proparr[0]][0][i])
							strclass += ' hasnullvalue';
						// no data && no trait
						else 
							strclass += ' no';
						dval.className = strclass
						dprop.appendChild(dval);
					}
					dinfo.className = 'dn propinfo';
					dinfo.textContent = proparr[1];
//					dinfo.appendChild(document.createElement('br'));
					dprop.appendChild(dinfo);

					const nbval = (val[3]['null']) ? val[3]['null'].length : 0;
					dinfo2.className = 'dn';
					dinfo2.textContent = query.LENGTH - nbval;
					dinfo.appendChild(dinfo2);

					dprop.className = 'prop prop-' + proparr[0] + '-' + proparr[1];
					dtraitbox.appendChild(dprop);
				}
			}
		}

//		document.getElementById('choice').appendChild(props);
		document.getElementById('content').textContent = '';
		document.getElementById('content').appendChild(vals);
	}
}

function displayval(val) {
	if (Array.isArray(val)) {
//		return val.join('\n');
		let strout = '',
			i = 1;
		for(const value of val) {
			strout += i + ': ';
			if (typeof value !== 'object')
				strout += value;
			else
				strout += JSON.stringify(value);
			strout += '\n';
			i++;
		}
		strout = strout.slice(0, -1);
		return strout;
	}
	else if (typeof val === 'object') {
		let strout = '';
		for(const [prop,value] of Object.entries(val)) {
			strout += prop + ': ';
			if (typeof value !== 'object')
				strout += value;
			else
				strout += JSON.stringify(value);
			strout += '\n';
		}
		strout = strout.slice(0, -1);
		return strout;
	}
	else if (typeof val == 'string' && val.startsWith('=')) {
		let out = val;
		const fn = f[getfnname(val)];
		if (fn && fn.desc) out += '\n/** ' + fn.desc + ' **/';
//		if (fn && fn.code) out += '\n' + 'function '+ val.substr(1) + ' {\n\t' + fn.code.trim(0,-1) + '\n}';
		return out;
	}
	else
		return val;
}

function createfromval(dom,val) {
	const out = document.createDocumentFragment();
	if (Array.isArray(val)) {
		for(const value of val) {
			const div = document.createElement('div');
			div.textContent = value
			out.appendChild(div);
		}
	}
	else if (typeof val === 'object') {
		for(const [prop,value] of Object.entries(val)) {
			const div = document.createElement('div');
			div.textContent = prop + ': ' +value;
			out.appendChild(div);
		}
	}
	else if (typeof val === 'string') {
		let isfunction = false,
			isimage = false,
			detailviewonly = false;
		const div = document.createElement('div');
//		div.setAttribute('type','button');
		if (val.startsWith('?')) {
			div.textContent = val;
			div.className = 'isquery';
		}
		else if (val.startsWith('url(')) {
			div.className = 'isimage';
			div.style.backgroundImage = val;
			div.textContent = val;
			isimage = true;
			detailviewonly = true;
		}
		else if (typeof val == 'string' && val.startsWith('=')) {
//		∫
			div.textContent = val;
			div.className = 'iscode';
			isfunction = true;
		}
		else
			div.textContent = val;

		div.addEventListener("contextmenu", function(ev) {
			// parent clicked
			if (ev.target === ev.currentTarget) {
				ev.preventDefault();

				const target = ev.target,
					textcontent = target.textContent;
				let input = null;

				if (isimage) {
					input = document.createElement('button');
					input.className = 'isdetail';
					input.style.backgroundImage = textcontent;
					input.textContent = textcontent;
				}
				else {
					if (textcontent.length > 20) {
						input = document.createElement('textarea');
						input.textContent = textcontent;
						input.setAttribute('spellCheck','false');
						input.className = 'isdetail';
					}
					else {
						input = document.createElement('input');
						input.setAttribute('type','text');
						input.value = textcontent;
					}
				}
				if (!detailviewonly) {
					target.textContent = '';
					target.className = 'edited';
				}

				input.addEventListener('focusout', function(ev) {
					const parent = ev.currentTarget.parentElement,
						target = ev.target;
					parent.textContent = target.value;
				});

				target.appendChild(input);
				input.focus();
				
				if (isfunction) {
					const fn = f[getfnname(textcontent)],
						textarea = document.createElement('textarea');
					if (fn && fn.code) textarea.textContent = fn.code;
					textarea.className = 'isdetail iscode';
					textarea.setAttribute('spellCheck','false');
					target.appendChild(textarea);
				}
			}
		});

		out.appendChild(div);
//		microlight.reset();
	}
	else {
		const div = document.createElement('div');
		div.textContent = val;
		out.appendChild(div);
	}

	
	dom.appendChild(out);
//	else if (typeof val === 'object') {
//		let strout = '';
//		for(const [prop,value] of Object.entries(val)) {
//			strout += prop + ': ';
//			if (typeof value !== 'object')
//				strout += value;
//			else
//				strout += JSON.stringify(val);
//			strout += ' | ';
//		}
//		strout = strout.slice(0, -3);

//	}
//	else if (typeof val == 'string' && val.startsWith('='))

//	else

}
//function createQuery(queryid) {
//	const queryarr = queryid.split('.');
//	if (queryarr.length == 2) {
//		const query = (queryarr[0] == 'app') ? q[queryarr[1]] : D[3][queryarr[1]],
//			identify = getarr(query,'identify.n');
//		f.switchview.obj('mode-explorer');
//		console.log(query);
//		const props = document.createElement('dl'),
//			vals = document.createDocumentFragment();
//		let dl2 = null;
//		for (let i = -1; i < query.LENGTH ; i++) {
//			const edom = document.createElement('dl');
//			let k = 0;
//			for (const [prop,val] of Object.entries(query)) {
//				if (prop != 'LENGTH') {
//					proparr = prop.split('.');
//					const div = document.createElement('div');
//					if (proparr.length == 1) {
//						div.className = 'trait';
//						if (i == -1) div.textContent = prop;
//					}
//					else {
//						if (i == -1) div.textContent = proparr[1];
//						else {
//							if (val[1][i]) div.textContent = ''+val[1][i];
//						}
//					}
//					div.style.gridArea = (k+1) + ' / ' + (i+2);
//					vals.appendChild(div);
//					k++;
//				}
//			}
//		}
////		document.getElementById('choice').appendChild(props);
//		document.getElementById('content').appendChild(vals);
//	}
//}

//function createQuery(queryid) { FLEX
//	const queryarr = queryid.split('.');
//	if (queryarr.length == 2) {
//		const query = (queryarr[0] == 'app') ? q[queryarr[1]] : D[3][queryarr[1]],
//			identify = getarr(query,'identify.n');
//		f.switchview.obj('mode-explorer');
//		
//		console.log(query);

//		const props = document.createElement('dl'),
//			vals = document.createDocumentFragment();
//		let dl2 = null;
//		for (let i = -1; i < query.LENGTH ; i++) {
//			const edom = document.createElement('dl');
//			for (const [prop,val] of Object.entries(query)) {
//				if (prop != 'LENGTH') {

//					proparr = prop.split('.');
//					if (proparr.length == 1) {
//						const dt = document.createElement('dt'),
//							dd = document.createElement('dd');
//						dl2 = document.createElement('dl');
//						dd.appendChild(dl2);

//						if (i == -1) {
//							dt.textContent = prop;
//							props.appendChild(dt);
//							props.appendChild(dd);
//						}
//						else {
//							dt.textContent = '-';
//							if (!val[0][i]) {
//								dt.className = 'visibilityhidden';
//								dd.className = 'visibilityhidden';
//							}
//							edom.appendChild(dt);
//							edom.appendChild(dd);
//						}

//					}
//					else {
//						const dt = document.createElement('dt'),
//							dd = document.createElement('dd');
//						if (i == -1) {
//							dt.textContent = proparr[1];
//						}
//						else {
//							dt.textContent = i + ':' + val[1][i];
//						}
//						dl2.appendChild(dt);
//						dl2.appendChild(dd);
//					}

//				}
//			}
//			vals.appendChild(edom);
//		}



//		document.getElementById('choice').appendChild(props);
//		document.getElementById('result').appendChild(vals);
//	}

//}

function keynametotext(val) {
	switch (val) {
		case ' ':
			return 'Space';
		case 'ArrowLeft':
			return '←';
		case 'ArrowRight':
			return '→';
		case 'ArrowUp':
			return '↑';
		case 'ArrowDown':
			return '↓';
		default:
			return val;
	}
}
function convertMarkdownHtml(markdown) {
	const htmlText = markdown
		.replace(/^# (.*$)/gim, '<h3>$1</h3>')
		.replace(/^## (.*$)/gim, '<h4>$1</h4>')
		.replace(/\*\*(.*)\*\*/gim, '<b>$1</b>')
		.replace(/\*(.*)\*/gim, '<i>$1</i>')
		.replace(/\-+ (.*)?/gim, '<ul><li>$1</li></ul>')
		.replace(/(\<\/ul\>\n(.*)\<ul\>*)+/gim,'')
		.replace(/\n$/gim, '<br />')
		.replace(/\[(.*?)\]\((.*?)\)/gim, "<a href='$2'>$1</a>");
	return htmlText;
}
