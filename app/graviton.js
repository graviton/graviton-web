'use strict';
let DEBUG = [];
let FILES = { app : [
{	T				: "startfn", // trait for lauching functions at start
	order			: { defaultvalue : 10 },
},
{	T				: "callesfn", // trait to call ecmascript functions
	n				: { execute : false }, // Prepare function without running it
//	desc			: { defaultvalue : "=getfndesc~e.callesfn.n" },
},
{	T				: "identify", // trait who give a identifier prop
	n				: {"hashmaxlen": 4},
	desc			: {},
},
{	T				: "activate", // trait 
},
{	T				: "archived", 
},
{	Q				: "init", // base
	filter			: [ { notequals : { "archived": true } } ],
	sorted			: [ {"identify.n": "up" } ],
	order			: [ "identify", "callesfn", "startfn", "*" ],
},
{	Q				: "start", // list of starting programs
	filter			: [ { equals : { "startfn": true } } ],
	sorted			: [ {"startfn.order": "up" } ],
},

],
data : []}; // original data FILES loaded

const g = []; // global data vars from data FILES

const f = {}; // functions declared with { F : name }

const t = {}; // traits and properties declared with { T : name, propname1 : val1, propname2... }

const d = {}; // traits and properties declared with { T : name, propname1 : val1, propname2... }

const q = {}; // query objects from data FILES

const D = [ {}, {}, {}, {} ]; // user data

const HASHMAXLEN = 6; // maximum string length to be indexed (hashed)
const ARRGEN = (len) => Array.from({length: len}, function(v,i) { return null});
const ARRGENi = (len) => Array.from({length: len}, function(v,i) { return i});
const ARRGEN20 = (len) => Array.from({length: len}, function(v,i) {
	return [];
});
const ARRGEN2lnull = (len, len2) => Array.from({length: len}, function(v,i) {
	const a = (len) => Array.from({length: len}, function(v,i) { return null});
	return a(len2);
});
function ERROR_NOFUNC(name="noname") { console.error("Graviton : function ["+name+"] not declared !"); }

// STARTING
document.addEventListener('DOMContentLoaded', (event) => {
	DEBUG = Array.isArray(DEBUG) ? DEBUG[0] : false;
	loaddata(FILES.app,"APPLICATION",f,t,d,q);
	loaddata(FILES.data,"DATA",D[0],D[1],D[2],D[3]);
	startfn();
//			if (t.length > 0) t = { t, ...tmpt }; else t = tmpt;
});

// UTILITY functions
// -----------------------------------------------------------
function isLetter(c) { return c.toLowerCase() != c.toUpperCase(); }
function isCharNumber(c) { return c >= '0' && c <= '9'; }
function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }
function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.
  // Please note that calling sort on an array will modify that array.
  // you might want to clone your array first.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}
function median(values){
  if(values.length ===0) return 0;

  values.sort(function(a,b){
    return a-b;
  });

  var half = Math.floor(values.length / 2);

  if (values.length % 2)
    return values[half];

  return (values[half - 1] + values[half]) / 2.0;
}
function standarddeviation (array,mean=null) {
  const n = array.length
  if (mean === null)
  	mean = array.reduce((a, b) => a + b) / n
  return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n)
}
function getfnname(expression, FNDELIM='~') {
	if (expression.startsWith('=')) expression = expression.substr(1);
//	console.log("cpicpi",expression,expression.split(FNDELIM));
	const exprarr = expression.split(FNDELIM);
	if (exprarr.length > 1)
		return exprarr[0];
	else
		return expression;
//	console.log("cpicpi",expression);
}
function stringtolang(string,lang="en") { // TODO clean way to handle multilanguage
	const stringarr = string.split('|');
	if (stringarr.length > 1)
		return stringarr[0];
	else
		return string;
}

// BASE functions
// -----------------------------------------------------------
// Initial function loader
function startfn() {
//	logg('----STARTFN----');
	const txt = q.start[1]['callesfn.n'][0];
	let i = 0;
	for (const func of q.start[1]['callesfn.n'][2]) {
		
		logg(txt[i]);
//		console.log(func[0]);
		func[0](...(typeof func[1] !== 'undefined' ? func[1] : ''));
		i++;
	}

//	if (DEBUG) console.log('----------------------------------------END CUSTOM STARTFN INIT------------------------------');
}

async function logg(txt,val=null,val2=null) {
	if (DEBUG) console.log(txt + ':',val,val2);

	if (val === null) val = '';
	if (val2 !== null) val = '('+val2+')';
	if (Array.isArray(val))
		val = txt+' ('+val.length+')';

	else if (typeof val === 'object')
		val = txt+' ('+Object.keys(val).length+')';
	else
		val = txt+' '+val;

	f.addcssvar.obj('txtloader',val+'\\A ');
}

// Pre-process data with special information (trait, executions, query)
function preprocess(e,val,tmpf,execute = true,FNDELIM='~') {
	let out = [val];
	if (typeof val === 'string') {
		const val2 = val.substr(1);
		const vstart = val.charAt(0);
		switch (vstart) {
			case "=":
				if (val2.includes('e.') && !val2.includes(FNDELIM)) {
					const paramfn = new Function('e','const x='+val2+';return x;');
//					console.log('preprocess e','const x='+val2+';return x;',paramfn(e),e);
					out.push(execute ? paramfn(e) : undefined);
					if (val2.includes('e.')) {
						const tmppar1 = val2.substr(val2.indexOf('e.'));
						let tmppar2 = '';
						let i = 0;
						while (isLetter(tmppar1.charAt(i)) || tmppar1.charAt(i) == '.') {
							tmppar2 += tmppar1.charAt(i);
							i++;
						}
						out.push([paramfn,tmppar2]);
					}
					else
						out.push([paramfn]);

	//				console.log("function with e",out);
				}
				else if (!val2.includes('e.') && !val2.includes(FNDELIM) && ( val2.startsWith("'") || val2.startsWith("(") || isCharNumber(val2.charAt(0)))) {
					const paramfn = new Function('const x='+val2+';return x;');
					out.push(execute ? paramfn() : null);
					out.push([paramfn]);
	//				console.log("function without e",out);
				}
				else {
					const separator = val2.indexOf(FNDELIM);
					if (separator != -1) {
						const fnname = val2.substr(0,separator);
						const param = val2.substr(separator+1).split(FNDELIM);
						for (let i = 0; i < param.length; i++) {
							if (isNumber(param[i]))
								param[i] = parseFloat(param[i]);
						}
	//					console.log("param",param);

						if (f.hasOwnProperty(fnname)) {
							out.push(execute ? tmpf[fnname].obj(val2.substr(separator+1)) : null);
							out.push([tmpf[fnname].obj,param]);
						}
						else {
							out.push(null);
							out.push([ERROR_NOFUNC,fnname]);
						}
					}
					else {
						if (f.hasOwnProperty(val2)) {
							out.push(execute ? tmpf[val2].obj() : null);
							out.push([tmpf[val2].obj]);
						}
						else {
							out.push(null);
							out.push([ERROR_NOFUNC,val2]);
						}
					}
				}
				break;
			case ":":
				break;
			case "?":
				break;
			default:
				out.push(val);	
				break;
		}
	}
	else {
		out.push(val);
		out.push(null);
	}
	return out;
}

// create hash of val to make faster filter equals and contains
function createhash(val,index,hashequ,hashcon=null, hashconmax=1) { //TODO ASYNC !

	// equals hashmap
	if (hashequ.hasOwnProperty(val)) {
//	console.log(hashequ,val,hashequ[val]);
		if (hashequ[val].indexOf(index) == -1)
			hashequ[val].push(index);
	}
	else
		hashequ[val] = [index];
	
	// create contains hashmap of hashconmax word length if param defined
	if (hashcon !== null || hashconmax == 0) {
		const firsttime = {};

		if (val !== null &&
			typeof val !== 'boolean' &&
			typeof val !== 'object' && // TODO array and object ?
			typeof val !== 'undefined') {

			val += '';
			val = val.toLowerCase(); // default value to lower case for memory optimisation. If differenciation needed, do calc in filter

			for (let nbletter = 1; nbletter <= hashconmax ; nbletter++) {
				const limit = val.length - nbletter + 1;
				for (let x = 0; x < limit; x++) {

					const word = val.substr(x,nbletter);

					if (hashcon.hasOwnProperty(word)) {
						if (!firsttime.hasOwnProperty(word)) {
							hashcon[word].push(index);
							firsttime[word] = false;
						}
					}
					else {
						hashcon[word] = [index];
						firsttime[word] = false;
					}
				}
			}
		}
	}
}

function cleanproperty(data) {
	const traitdeleted = {};
	for (const [propname, propval] of Object.entries(data)) {
	
		// clean property with no data
		const propnamearr = propname.split('.');
		if (propnamearr.length > 1) {
			let notnull = 0;
			const propvallen = propval[0].length;
			for (let i = 0; i < propvallen; i++)
				if (propval[0][i] !== null || propval[1][i] !== null) notnull++;
			if (notnull == 0 || traitdeleted[propnamearr[0]])
				delete data[propname];
		}

		// clean trait never used
		else {
			let notfalse = 0;
			for (const val of propval[0])
				if (val) notfalse++;
			if (notfalse == 0) {
				delete data[propname];
				traitdeleted[propname] = true;
			}
		}
	}
}

function loadquery(e,data,traits,clean=true) {
	const tmpq = [];
//console.log("in",data,e);


	// FILTERS
	let filterarr = ARRGENi(data.LENGTH);
	if (e.hasOwnProperty('filter')) {
		for (const element of e.filter) {

			for (const [typename, typevalue] of Object.entries(element)) {
				const tmparr = [];
				let minarr = null;

				for (const [filtertrait, filtervalue] of Object.entries(typevalue)) {
//console.log(data[filtertrait],filtertrait,data);

					// No filtertrait present in data
					if (!data.hasOwnProperty(filtertrait)) {
						if (filtertrait.indexOf('.') == -1) {
							let cur = null;

							if (typename == 'notequals')
								cur = filterarr;

							if (minarr === null)
								minarr = cur;
							else {
								// test the smallest array index if new is smallest, move old in tmparr
								if (minarr.length > cur.length) {
									tmparr.push(minarr);
									minarr = cur;
								}
								else tmparr.push(cur);
							}

						}
					}
					// trait and property has been declared
					else {

						// trait only
						if (filtertrait.indexOf('.') == -1) {
							let cur = null;

							if (typename == 'notequals')
								cur = data[filtertrait][1].false;
							else
								cur = data[filtertrait][1].true;

							if (minarr === null)
								minarr = cur;
							else {
								// test the smallest array index if new is smallest, move old in tmparr
								if (minarr.length > cur.length) {
									tmparr.push(minarr);
									minarr = cur;
								}
								else tmparr.push(cur);
							}
						}
						
						// props
						else {
							const filtervaluelc = filtervalue.toLowerCase(),
								filtertraitarr = filtertrait.split('.'),
								traitname = filtertraitarr[0],
								propname = filtertraitarr[1],
								hashmaxlen = traits[traitname+'.'+propname].hasOwnProperty('hashmaxlen') ? traits[traitname+'.'+propname].hashmaxlen : HASHMAXLEN;
							let cur = null,
								tmp = null;
							if (typename == 'equals') {
								cur = data[filtertrait][3][filtervalue];
								if (typeof cur === 'undefined') cur = [];
							}
							else if (filtervalue.length <= hashmaxlen) {
								switch (typename) {
									case 'contains':
										tmp = data[filtertrait][4][filtervaluelc];
										cur = [];
										if (typeof tmp !== 'undefined') {
											for (const index of tmp) {
												const val = data[filtertrait][1][index] + '';
												if (val.indexOf(filtervalue) != -1)
													cur.push(index);
											}
										}
										break;
									case 'icontains':
										cur = data[filtertrait][4][filtervaluelc];
										if (typeof cur === 'undefined') cur = [];
										break;
									case 'iequals':
										tmp = data[filtertrait][4][filtervaluelc];
										cur = [];
										if (typeof tmp !== 'undefined') {
											for (const index of tmp) {
												const val = data[filtertrait][1][index] + '';
												if (val.toLowerCase() == filtervaluelc)
													cur.push(index);
											}
										}
										break;
								}
							}
							else {
								const prop = data[filtertrait][1],
									datacount = prop.length;
								cur = [];
//													console.log(prop,filtervalue);
								for (let x = 0; x < datacount ; x++) {
									if (queryfiltertype(typename,prop[x],filtervalue))
										cur.push(x);
								}
							}
							if (minarr === null)
								minarr = cur;
							else {
								// test the smallest array index if new is smallest, move old in tmparr
								if (minarr.length > cur.length) {
									tmparr.push(minarr);
									minarr = cur;
								}
								else tmparr.push(cur);
							}
						}
						if (DEBUG) console.log("filter :",typename,filtertrait,filtervalue,tmparr,minarr);
					}
				}
				// do intersection of index found in tmparr and minarr
				if (tmparr.length > 0) {
					filterarr = [];
//				console.log("truc",tmparr,minarr);
					for (const minval of minarr) {
						let found = false;
						for (const arr of tmparr) {
							for (const val of arr) {
								if (val == minval) {
									found = true;
									break;
								}
								else if (val > minval)
									break;
							}
							if (!found)
								break;
						}
						if (found) {
							filterarr.push(minval);
						}
					}
				}
				else
					filterarr = minarr;
				console.log("filter intersection : ",filterarr);
			}
		}
	}

	// SORT
	if (e.hasOwnProperty('sorted') && filterarr.length > 1) {
		for (const element of e.sorted) {
			for (const [tprop, sortdir] of Object.entries(element)) {

				if (data.hasOwnProperty(tprop)) {
//															console.log(traitarr[0],traitarr[1],itrait,iprop)
					const prop = data[tprop][1],
						datacount = filterarr.length,
						sortedarr = ARRGEN(datacount);

					// add index to elements to keep position after sort
					for (let x = 0; x < datacount ; x++)
							sortedarr[x] = [prop[filterarr[x]],filterarr[x]];
//console.log(sortedarr);
					// sort up or down
					if (sortdir == 'up')
						sortedarr.sort(function(a, b){return (a[0]===null)-(b[0]===null) || +(a[0]>b[0])||-(a[0]<b[0]);});
					else if (sortdir == 'down')
						sortedarr.sort(function(a, b){return (a[0]===null)-(b[0]===null) || +(a[0]<b[0])||-(b[0]>a[0]);});
					
					// cleaning values used to sort and keep index only
					for (let x = 0; x < datacount ; x++)
							filterarr[x] = sortedarr[x][1];

					if (DEBUG) console.log("sort detail",tprop, sortdir, prop, filterarr,sortedarr);
				}
			}
		}
	}
	tmpq.push(filterarr);

//console.log("filterarr",filterarr);
	// COMPLETING DATA
	if (filterarr !== null) {

		const tmpqd = {};
		let i = 0,
			tmpdata = {...data};
		// Ordering data liste if ORDER entry
		if (e.hasOwnProperty('order') && e.order.length > 0) {
			const tmpdatafirst = {},
				tmpdataend = {};
			let hasasterisk = false,
				tmpdatacur = null;
			for (const prop of e.order) {
				if (prop != '*' && tmpdata[prop]) {
					if (prop != '*' && !hasasterisk)
						tmpdatacur = tmpdatafirst;
					else if (prop != '*' && hasasterisk)
						tmpdatacur = tmpdataend;
					else
						hasasterisk = true;

					if (prop != '*') {
						const proparr = prop.split('.');
						tmpdatacur[prop] = tmpdata[prop];
						delete tmpdata[prop];
						if (proparr.length == 1) {
							for (const prop2 of Object.keys(tmpdata)) {
								const proparr2 = prop2.split('.');
								if (proparr2.length == 2 && proparr2[0] == proparr[0]) {
									tmpdatacur[prop2] = tmpdata[prop2];
									delete tmpdata[prop2];
								}
							}
						}
					}
				}
			}
			tmpdata = {...tmpdatafirst,...tmpdata,...tmpdataend};
		}
		console.log('tmpdata',tmpdata);
		for (const [prop, vals] of Object.entries(tmpdata)) {
			if (prop != "LENGTH") {

				const subarrd = ARRGEN(tmpqd.LENGTH),
					subarrc = ARRGEN(tmpqd.LENGTH),
					subarrf = ARRGEN(tmpqd.LENGTH),
					subarrh = {},subarrhc = {},
					sumarr = {sum: null, avg: null, min: null, max: null};
				let isnumeric = true,
					j = 0;
				for (const index of filterarr) {
//					console.log(tmpdata,vals);
					if (vals.length > 2) {
						const hashmaxlen = traits[prop].hasOwnProperty('hashmaxlen') ? traits[prop].hashmaxlen : HASHMAXLEN;
						const value = vals[1][index];
						subarrd[j] = vals[0][index];
						subarrc[j] = value;
						subarrf[j] = vals[2][index];
						createhash(vals[1][index], j, subarrh, subarrhc, hashmaxlen);
						if (typeof value !== 'number')
							isnumeric = false;
					}
					else {
						subarrd[j] = vals[0][index];
						createhash(vals[0][index], j, subarrh);
					}
					j++;
				}

				// calculate CONSOLIDATION data 
				if (isnumeric && e.hasOwnProperty('summary')) {
					for (const [sprop, formularr] of Object.entries(e.summary)) {
						if (prop == sprop) {
							for (const form of formularr) {
		//										console.log("coucou",typeof value,value,form,sumarr[form]);
								switch (form) {
									case 'summ':
									sumarr[form] = subarrc.reduce((a, b) => a + b);
									break;
									case 'mean':
									sumarr[form] = sumarr.sum !== null ? sumarr.summ / subarrc.length : subarrc.reduce((a, b) => a + b) / subarrc.length;
									break;
									case 'min':
									sumarr[form] = subarrc.reduce((a, b) => Math.min(a, b));
									break;
									case 'max':
									sumarr[form] = subarrc.reduce((a, b) => Math.max(a, b));
									break;
									case 'median':
									sumarr[form] = median(subarrc);
									break;
									case 'stddeviation':
									sumarr[form] = sumarr.mean !== null ? standarddeviation(subarrc, sumarr.mean) : standarddeviation(subarrc);
									break;
									
								}
							}
						}
					}
				}

				if (vals.length > 2)
					tmpqd[prop] = [subarrd,subarrc,subarrf,subarrh,subarrhc,sumarr];
				else
					tmpqd[prop] = [subarrd,subarrh];
				i++;
			}

		}
	//					console.log(Object.keys(tmpqd).length);
		if (clean) cleanproperty(tmpqd);
		tmpqd['LENGTH'] = filterarr.length;
	//					console.log(Object.keys(tmpqd).length);
	//					console.log("tmpqd",tmpqd);
		tmpq.push(tmpqd);
	}
//console.log("out",tmpq);
	return tmpq;
}

// Load local settings from database for fast access
function loaddata(data,txtlabel,tmpf,tmpt,tmpd,tmpq,merge=1) {
	if (DEBUG) console.log('----------------------------------------START LOADING '+txtlabel+'----------------------------------------');
	const FNDELIM = '~';
console.log(data);
    // initialize temp data view
    const tmpdata = { F : [], T : [], Q : [], D : [], G : []};
	for (const e of data) {
		if (e.hasOwnProperty('F'))		tmpdata.F.push(e);
		else if (e.hasOwnProperty('T'))	tmpdata.T.push(e);
		else if (e.hasOwnProperty('Q'))	tmpdata.Q.push(e);
		else if (e.hasOwnProperty('G'))	tmpdata.G.push(e);
		else							tmpdata.D.push(e);
	}

	// prepare GLOBAL from data
	if (tmpdata.G.length > 0) {
		g.push({});
		const gindex = g.length - 1;
		for (const e of tmpdata.G) {
			for (const [key, val] of Object.entries(e)) {
				if (key != 'G')
					g[gindex][key] = val;
			}
		}
	}
	if (DEBUG) console.log('g:',g);


	// prepare FUNCTION from data
	for (const e of tmpdata.F) {
		const fnname = e.n;
		delete e.F;
		tmpf[fnname] = e;
		
		// load inline code
		if (e.code) {
			let tmpinput = [];
			let tmpcode = e.code;
			let tmpheader = '';
//			tmpheader += (DEBUG) ? 'console.log("Custom function : '+fnname+'");' : '';
			let tmpfooter= '';

			if (e.input) {
				let i = 0;
				for (const val of e.input) {
					// if param name include reference to entity object, modify prototype and var names
					if (val.includes('.')) {
						tmpcode = tmpcode.replaceAll(val,'var' + i);
						const tmpval = val.substr(0,val.indexOf('.'));

						if (tmpinput.includes(tmpval)) {
							tmpinput.push(tmpval+i);
							tmpheader += 'const var'+i+'=((typeof '+tmpval+i+'==="undefined" && typeof '+tmpval+'==="object")||typeof '+tmpval+i+'==="object")?'+val+':'+tmpval+i+';';
						}
						else {
							tmpinput.push(tmpval);
							tmpheader += 'const var'+i+'=(typeof '+tmpval+'==="object")?'+val+':'+tmpval+';';
						}
					}
					else tmpinput.push(val);
					i++;
				}
			}
//				console.log(tmpinput,"new function",tmpinput.join(','),tmpheader + tmpcode);
			const tmpfn = new Function(tmpinput.join(','),tmpheader + tmpcode + tmpfooter);
			tmpf[fnname].obj = tmpfn;
			
		}
		// load external code TODO remove for more security in future
		else
			if (typeof window[fnname] === 'function')
				tmpf[fnname].obj = window[fnname];
			else
				tmpf[fnname].obj = null;
	}
	logg('----'+txtlabel+'----');
	logg('functions',tmpf);


	// load TRAIT type
	let i = 0, j = 0, nbprop = 0;
	if (tmpdata.T.length > 0) {
		for (const e of tmpdata.T) {
			const name = e.T;
			delete e.T;
			tmpt[name] = true;
			for (const [prop,val] of Object.entries(e)) {
				tmpt[name+'.'+prop] = val;
			}
		}
	}
	logg('trait / properties',tmpt);


//console.log("tmpdata.D",tmpdata.D);
	// load NORMAL data type
	if (tmpdata.D.length > 0) {
		i = 0;
		j = 0;
		let hash = {}, // hash to optimize "equals" filter
			hasc = {}, // hash to optimize "contains" filter
			hastrait = false;
		let tmpobj = {};
		for (const e of tmpdata.D) {
			tmpobj = {};
			hastrait = false;
			for (const [tname, tpropval] of Object.entries(tmpt)) {
//console.log("i",i);
				const tnamearr = tname.split('.');

				// Data is property
				if (tnamearr.length > 1 && hastrait) {

					const curtrait = tnamearr[0],
						curprop = tnamearr[1],
						hashmaxlen = tmpt[tname].hasOwnProperty('hashmaxlen') ? tmpt[tname].hashmaxlen : HASHMAXLEN,
						execute = !tmpt[tname].hasOwnProperty('execute') ?? tmpt[tname].execute;
					let vdata = null, vcach = null, vfunc = null;

					// data provided
					if (e.hasOwnProperty(curtrait) && e[curtrait].hasOwnProperty(curprop)) {
						const val = e[curtrait][curprop];
						[vdata,vcach,vfunc] = preprocess(e,val,tmpf,execute); // TODO manage if you want explicitly not the default value and the value is unknown 
					}
					// No data but default value defined in trait
					else {
						if (tmpt[tname].hasOwnProperty('defaultvalue')) {
							const val = tmpt[tname].defaultvalue;
//							console.log(tname,tnamearr[0],tnamearr[1],e,vcach);
							[vdata,vcach,vfunc] = preprocess(e,val,tmpf,execute);
							// complete initial data with calculated default values
							if (typeof e[tnamearr[0]] !== 'boolean')
								e[tnamearr[0]][tnamearr[1]] = vcach;
							else
								e[tnamearr[0]] = {[tnamearr[1]]: vcach};
							vdata = null;
						}
					}
					tmpobj[tname] = [vdata,vcach,vfunc];
				}

				// Data is trait activation
				else {
					hastrait = e.hasOwnProperty(tname);
					tmpobj[tname] = hastrait;
				}
			}
			
			// After all properties has been tested
			
			// test if data is mergeable
			let mergeable = false;
			if (merge > 0 && tmpobj['identify.n'] && tmpd['identify.n']) {
				const propshash = tmpd['identify.n'][3],
					proptotest = tmpobj['identify.n'][1];
				if (propshash.hasOwnProperty(proptotest)) {
					const jarr = propshash[proptotest];
					if (jarr.length == 1) {
						const j = jarr[0];
						mergeable = true;
						for (const prop of Object.keys(tmpobj)) {
							if (prop.indexOf('.') != -1 && prop != 'identify.n') {
								if (tmpd[prop][0][j] !== null && tmpobj[prop][0] !== null) {
//							console.log(tmpd[prop][0][j],tmpobj[prop]);
									mergeable = false;
									break;
								}
							}
						}
					}
				}
			}
			if (mergeable) {
//console.log("i",i,"mergeable",mergeable,tmpobj);
				for (const [prop, val] of Object.entries(tmpobj)) {
					if (prop != 'identify.n') {
						const proparr = prop.split('.');
						if (proparr.length == 1) {
							tmpd[prop][0][j] = val;
							createhash(val, j, tmpd[prop][1]);
						}
						else {
							const hashmaxlen = tmpt[prop].hasOwnProperty('hashmaxlen') ? tmpt[prop].hashmaxlen : HASHMAXLEN;
							if (tmpobj[proparr[0]] && val[0] !== null) {
								tmpd[prop][0][j] = val[0];
								tmpd[prop][1][j] = val[1];
								tmpd[prop][2][j] = val[2];
								createhash(val[1], j, tmpd[prop][3], tmpd[prop][4], hashmaxlen);
							}
						}
					}
				}
			}
			else {
				for (const [prop,val] of Object.entries(tmpobj)) {
					const proparr = prop.split('.');
					if (proparr.length == 1) {
						if (!tmpd[prop]) tmpd[prop] = [[],{}];
						tmpd[prop][0].push(val);
						createhash(val, i, tmpd[prop][1]);
					}
					else {
						const hashmaxlen = tmpt[prop].hasOwnProperty('hashmaxlen') ? tmpt[prop].hashmaxlen : HASHMAXLEN;
						if (!tmpd[prop]) tmpd[prop] = [ [], [], [], {}, {} ];
						if (tmpobj[proparr[0]]) {
							tmpd[prop][0].push(val[0]);
							tmpd[prop][1].push(val[1]);
							tmpd[prop][2].push(val[2]);
							createhash(val[1], i, tmpd[prop][3], tmpd[prop][4], hashmaxlen);
						}
						else {
							tmpd[prop][0].push(null);
							tmpd[prop][1].push(null);
							tmpd[prop][2].push(null);
							createhash(null, i, tmpd[prop][3]);
						}
					}
				}
				i++;
			}
		}
//				console.log(tmpd,tmpobj);

		// Clean property not used
		cleanproperty(tmpd);
		tmpd['LENGTH'] = i;

		logg('base data',tmpd,i);
//		if (DEBUG) console.log('dclean:', d);
	}

	// load base QUERY type
	if (tmpdata.Q.length > 0) {
		i = 0;
		// search init query (base for other query)
		for (const e of tmpdata.Q) {
			if (e.Q == 'init') {
				tmpq['init'] = loadquery(e,tmpd,tmpt)[1];
//				console.log("query init",tmpq.init);				
//				throw new Error("Graviton : NO INIT QUERY, ABORT");
				break;
			}
			i++
		}
		if (i == tmpdata.Q.length) {
			logg('ERROR : NO APPLICATION INIT QUERY, ABORT!');
			throw new Error("Graviton : NO APPLICATION INIT QUERY, ABORT!");
		}
		for (const e of tmpdata.Q) {
			const name = e.Q;
			delete e.Q;

			// ignore trait with same name
			if (!tmpq.hasOwnProperty(name)) {
				if (name != 'init') {
					if (DEBUG) console.log('loading query [' + name + ']______________');
					tmpq[name] = loadquery(e,tmpq.init,tmpt);
				}
			}
		}
	}
	logg('query',tmpq);

	if (DEBUG) console.log('----------------------------------------END LOADING '+txtlabel+'----------------------------------------');
}


function queryfiltertype(filtertype,x,y) { //TODO to optimize if needed, automatic insensitive case type if lowercase input
//console.log(x,y);
	let out = false;
	let lx = x, ly = y;
	if (filtertype.startsWith('i')) {
		if (typeof x === 'string')
			lx = x.normalize().toUpperCase();
		else if (Array.isArray(x))
			lx = x.map(val => val.normalize().toUpperCase());

		if (typeof y === 'string')
			ly = y.normalize().toUpperCase();
		else if (Array.isArray(y))
			ly = y.map(val => val.normalize().toUpperCase());
	}
	if (isNumber(lx))
		lx = parseFloat(lx);

	if (isNumber(ly))
		ly = parseFloat(ly);

//console.log(filtertype,lx,ly);
	switch (filtertype) {
		case 'equals':
		case 'iequals':
			if (Array.isArray(lx) && arraysEqual(lx, ly))
				out = true;
			else if (lx == ly)
				out = true;
			break;
		
		case 'contains':
		case 'icontains':
			if ((typeof lx === 'string' || Array.isArray(lx)) &&  lx.indexOf(y) != -1)
				out = true;
			else if (typeof lx === 'number' && lx < ly)
				out = true;
//			else if (lx < ly)
//				out = true;
			break;

		case 'lower':
		case 'ilower':
			if ((typeof lx === 'string' && typeof ly === 'string' || typeof lx === 'number' && typeof ly === 'number' ) && lx < ly)
				out = true
			else if (Array.isArray(lx) && (typeof ly === 'string' || typeof ly === 'number')) {
				for (const valx of lx) {
					if (valx < ly) {
						out = true;
						break;
					}
				}
			}
//			if (Array.isArray(lx) && Array.isArray(ly)) TODO ?

			break;

		case 'greater':
		case 'igreater':
			if ((typeof lx === 'string' && typeof ly === 'string' || typeof lx === 'number' && typeof ly === 'number' ) && lx > ly)
				out = true
			else if (Array.isArray(lx) && (typeof ly === 'string' || typeof ly === 'number')) {
				for (const valx of lx) {
					if (valx > ly) {
						out = true;
						break;
					}
				}
			}
//			if (Array.isArray(lx) && Array.isArray(ly)) TODO ?

			break;
	}
//console.log("out",out);
	return out;
}
